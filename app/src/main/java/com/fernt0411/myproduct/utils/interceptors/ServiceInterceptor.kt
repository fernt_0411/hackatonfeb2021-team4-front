package com.fernt0411.myproduct.utils.interceptors

import android.content.SharedPreferences
import com.fernt0411.myproduct.data.b_data.security.SecurityRepository
import com.fernt0411.myproduct.data.d_usecase.security.GetTokenLocalUseCase
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject


class ServiceInterceptor @Inject constructor(
  private val sharedPreferences: SharedPreferences
) : Interceptor {


    var token: String = sharedPreferences.getString("token", String()).toString()


    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if (request.header("No-Authentication") == null) {

            //or use Token Function
            if (!token.isNullOrEmpty()) {
                val finalToken = "Bearer $token"
                request = request.newBuilder()
                    .addHeader("Authorization", finalToken)
                    .build()
            }

        }

        return chain.proceed(request)
    }

}