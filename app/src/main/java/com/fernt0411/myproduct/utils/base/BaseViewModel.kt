package com.fernt0411.myproduct.utils.base

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.fernt0411.myproduct.data.d_usecase.security.ResetLocalUseCase
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCase

class BaseViewModel @ViewModelInject constructor(

    private val resetLocalUseCase: ResetLocalUseCase,
    private val getUserLocalUseCase: GetUserLocalUseCase

) : ViewModel() {


    fun resetLocal() {
        return resetLocalUseCase.invoke()
    }

    fun isThereUserLocal(): Boolean {


        return getUserLocalUseCase.invoke().first_name.isNotEmpty()
    }

}