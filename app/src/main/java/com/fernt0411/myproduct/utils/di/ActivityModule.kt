package com.fernt0411.myproduct.utils.di

import com.fernt0411.myproduct.data.a_framework.auth.AuthNetwork
import com.fernt0411.myproduct.data.a_framework.auth.AuthNetworkImpl
import com.fernt0411.myproduct.data.a_framework.dashboard.DashboardNetwork
import com.fernt0411.myproduct.data.a_framework.dashboard.DashboardNetworkImpl
import com.fernt0411.myproduct.data.a_framework.security.SecurityLocal
import com.fernt0411.myproduct.data.a_framework.security.SecurityLocalImpl
import com.fernt0411.myproduct.data.a_framework.user.UserLocal
import com.fernt0411.myproduct.data.a_framework.user.UserLocalImpl

import com.fernt0411.myproduct.data.b_data.auth.AuthRepository
import com.fernt0411.myproduct.data.b_data.auth.AuthRepositoryImpl
import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepositoryImpl
import com.fernt0411.myproduct.data.b_data.security.SecurityRepository
import com.fernt0411.myproduct.data.b_data.security.SecurityRepositoryImpl
import com.fernt0411.myproduct.data.b_data.user.UserRepository
import com.fernt0411.myproduct.data.b_data.user.UserRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent


@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class ActivityModule {

    //Auth

    @Binds
    abstract fun bindAuthNetworkImpl(authNetworkImpl: AuthNetworkImpl): AuthNetwork

    @Binds
    abstract fun bindAuthRepositoryImpl(authRepositoryImpl: AuthRepositoryImpl): AuthRepository


    //User

    @Binds
    abstract fun bindUserLocalImpl(userLocalImpl: UserLocalImpl): UserLocal

    @Binds
    abstract fun bindUserRepositoryImpl(userRepositoryImpl: UserRepositoryImpl): UserRepository


    //Security

    @Binds
    abstract fun bindSecurityLocalImpl(securityLocalImpl: SecurityLocalImpl): SecurityLocal

    @Binds
    abstract fun bindSecurityRepositoryImpl(securityRepositoryImpl: SecurityRepositoryImpl): SecurityRepository


    //Dashboard

    @Binds
    abstract fun bindDashboardLocalImpl(dashboardLocalImpl: DashboardNetworkImpl): DashboardNetwork

    @Binds
    abstract fun bindDashboardRepositoryImpl(dashboardRepositoryImpl: DashboardRepositoryImpl): DashboardRepository



}