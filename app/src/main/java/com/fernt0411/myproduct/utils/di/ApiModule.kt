package com.fernt0411.myproduct.utils.di

import com.fernt0411.myproduct.data.a_framework.auth.AuthApi
import com.fernt0411.myproduct.data.a_framework.dashboard.DashboardApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)//Todas las instancias estan creadas y sobreviven(ciclo de vida application) bajo este escope(application scope)
object ApiModule {


    @Singleton
    @Provides
    fun provideAuthApi(@AppModule.WithoutAuthorization retrofit: Retrofit): AuthApi =
        retrofit.create(AuthApi::class.java)

    @Singleton
    @Provides
    fun provideDashboardApi(@AppModule.WithAuthorization retrofit: Retrofit): DashboardApi =
        retrofit.create(DashboardApi::class.java)


}