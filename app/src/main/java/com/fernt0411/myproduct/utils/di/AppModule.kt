package com.fernt0411.myproduct.utils.di

import android.content.Context
import android.content.SharedPreferences
import com.fernt0411.myproduct.utils.constant.ApiConstants.URL_API
import com.fernt0411.myproduct.utils.interceptors.ServiceInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)//Todas las instancias estan creadas y sobreviven(ciclo de vida application) bajo este escope(application scope)
object AppModule {


    @Qualifier  // define qualifier for LoginRetrofitClient
    @Retention(AnnotationRetention.BINARY)
    annotation class WithoutAuthorization

    @Qualifier // define qualifier for OtherRetrofitClient
    @Retention(AnnotationRetention.BINARY)
    annotation class WithAuthorization




    @WithAuthorization
    @Singleton
    @Provides

    fun provideRetrofitWithAuthorization(gson: Gson,client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(URL_API)
           // .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    @WithoutAuthorization
    @Singleton
    @Provides
    fun provideRetrofitWithoutAuthorization(gson: Gson): Retrofit =
        Retrofit.Builder()
            .baseUrl(URL_API)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()


    @Singleton
    @Provides
    fun provideOkHttpClient(preferences: SharedPreferences): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(ServiceInterceptor(preferences))
            //.readTimeout(45,TimeUnit.SECONDS)
            //.writeTimeout(45,TimeUnit.SECONDS)
            .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder()
        .create()

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {

        return context.getSharedPreferences(
            "key", Context.MODE_PRIVATE
        )
    }


}