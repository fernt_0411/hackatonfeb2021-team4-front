package com.fernt0411.myproduct.utils.ui

import android.app.ProgressDialog
import android.content.Context

@Suppress("DEPRECATION")
class LoadingDialog(
    private var context: Context?,
    private var title: String? = "",
    private var message: String = "Espere...",

    ) {

    // private lateinit var progressDialog: ProgressDialog
    var progressDialog = ProgressDialog(context)


    fun show() {
        progressDialog.setTitle(title)
        progressDialog.setMessage(message)
        progressDialog.setCancelable(false);
        progressDialog.show()
    }

    fun hide() {

        progressDialog.dismiss()


    }

}