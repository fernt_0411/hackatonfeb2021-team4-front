package com.fernt0411.myproduct.utils.ui


import android.view.View
import android.widget.RadioGroup


//region View

typealias ItemClickListener<T> = (position: Int, data: T) -> Unit
typealias ItemOnCheckedRadioGroup = (position: String, radioGroup: RadioGroup) -> Unit
typealias ItemClickDialog<T> = (data: T) -> Unit

fun View.onClick(onClick: () -> Unit) {
    setOnClickListener { onClick() }
}

fun RadioGroup.onCheckedRadioGroup(onCheckedRadioGroup: () -> Unit) {
    setOnCheckedChangeListener { _: RadioGroup, i: Int -> onCheckedRadioGroup() }
}
