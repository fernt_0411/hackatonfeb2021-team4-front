package com.fernt0411.myproduct.utils.response

import android.util.Log

import retrofit2.Response


abstract class NetworkResponse {

    protected suspend inline fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {

        try {
            val response = call()

            if (response.isSuccessful) {
                val body = response.body()

                if (body != null) {

                    return Resource.success(body)
                }

            }

            return Resource.error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return Resource.error(e.message ?: e.toString())
        }
    }

    private fun <T> error(e: Exception): Resource<T> {
        Log.e("remoteDataSource", e.toString())
        return Resource.error(e.message.toString())
    }


}