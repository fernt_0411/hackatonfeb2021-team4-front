package com.fernt0411.myproduct.utils.base

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.utils.ui.LoadingDialog
import com.fernt0411.myproduct.utils.ui.LoadingFullscreenDialog


open class BaseActivity : AppCompatActivity() {

    val baseViewModel: BaseViewModel by viewModels()

    private lateinit var loadingDialog: LoadingDialog
    private lateinit var loadingFullscreenDialog: LoadingFullscreenDialog

    private lateinit var resetSessionDialog: MaterialDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        builderLoadingDialog();
        builderLoadingFullScreenDialog()


    }


    private fun builderLoadingDialog(title: String? = null) {
        loadingDialog = LoadingDialog(this, title, message = "Espere...")
    }

    private fun builderLoadingFullScreenDialog(title: String? = null) {
        loadingFullscreenDialog = LoadingFullscreenDialog(this, R.style.AppTheme)
    }


    fun showLoader() {
        loadingDialog.show()
    }

    fun hideLoader() {

        loadingDialog.hide()
    }

    fun showFullscreenLoader() {
        loadingFullscreenDialog.show()

    }

    fun hideFullscreenLoader() {
        loadingFullscreenDialog.hide()
    }

    fun showDialogSession() {

        resetSessionDialog = MaterialDialog(this).show {
            title(R.string.close_session)
            message(text = "Por seguridad. Tu sesión esta por expirar. ¿Desea continuar?")
            cancelable(false)

            positiveButton(R.string.close_session_yes) { dialog ->

                dialog.dismiss()
            }

            negativeButton(R.string.close_session_no) { dialog ->
                resetSessionDialog.dismiss()
            }


        }
    }


}