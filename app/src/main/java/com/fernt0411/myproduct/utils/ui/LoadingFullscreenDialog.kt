package com.fernt0411.myproduct.utils.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.LinearLayout
import com.fernt0411.myproduct.R

class LoadingFullscreenDialog(context: Context, theme: Int) : Dialog(context, theme) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_loading_fullscreen)
        if (window != null) {
            window!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
        }
        setCancelable(false)
    }

    fun showViewLoading() {
        if (!isShowing) {
            show()
        }
    }

    fun hideViewLoading() {
        if (isShowing) {
            dismiss()
        }
    }

}