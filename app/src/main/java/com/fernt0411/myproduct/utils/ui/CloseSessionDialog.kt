package com.fernt0411.myproduct.utils.ui

import android.app.AlertDialog
import android.content.Context

class CloseSessionDialog(context: Context) {

    val builder = AlertDialog.Builder(context)

    init {
        createDialog()
    }

    fun showDialog() {
        builder.setTitle("Androidly Alert")
        builder.setMessage("We have a message")
        builder.setPositiveButton(android.R.string.yes)
        { dialog, which ->

        }

        builder.setNegativeButton(android.R.string.no)
        { dialog, which ->

        }


        builder.show()
    }

    fun createDialog() {
    return
    }
}
