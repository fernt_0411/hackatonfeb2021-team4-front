package com.fernt0411.myproduct.utils.constant

object ApiConstants {
    //const val URL_API = "http://35.192.80.171/bootcamp/wp-json/bcp/"
    const val URL_API = "http://70.37.86.87:8088/donation/"
    const val AUTH = "login/"
    const val RESUME = "resume/"
    const val EXCHANGE = "exchange/"
    const val HISTORY = "history/"
    const val LOAN = "loan/"
    const val INQUIRY = "inquiry/"
    const val CARD = "card/"
    const val PUBLICATION ="Publicaciones"
    const val CREATE_PUBLICATION ="savepublicaciones1"




}