package com.fernt0411.myproduct.utils.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.fernt0411.myproduct.BR


abstract class BaseFragment<VM : ViewModel, DB : ViewDataBinding>() :
    Fragment() {

    @LayoutRes
    abstract fun getLayoutRes(): Int

    open lateinit var binding: DB
    protected abstract val viewModel: VM


    private fun setBinding(inflater: LayoutInflater, container: ViewGroup) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
    }

    private fun setViewModel() {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()
    }

    open fun initialize() {}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setBinding(inflater, container!!)
        setViewModel()
        initialize()
        super.onCreateView(inflater, container, savedInstanceState)
        return binding.root
    }


    fun showLoader() {
        with(activity) {
            if (this is BaseActivity) this.showLoader()
        }

    }

    fun hideLoader() {
        with(activity) {
            if (this is BaseActivity) this.hideLoader()
        }

    }


    fun showFullscreenLoader() {
        with(activity) {
            if (this is BaseActivity) this.showFullscreenLoader()
        }

    }

    fun hideFullscreenLoader() {
        with(activity) {
            if (this is BaseActivity) this.hideFullscreenLoader()
        }

    }


}