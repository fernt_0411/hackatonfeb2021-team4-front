package com.fernt0411.myproduct.utils.validators

import android.util.Patterns

object FormValidators {
    fun isDniValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun minAndMax(min: Int? = 0, max: Int? = 30, word: String): Boolean {

        var size = word.length

        return size >= min!! && size <= max!!
        //  return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

}