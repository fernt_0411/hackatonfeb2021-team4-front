package com.fernt0411.myproduct.utils.di.usecase

import com.fernt0411.myproduct.data.d_usecase.auth.LoginUseCase
import com.fernt0411.myproduct.data.d_usecase.auth.LoginUseCaseImpl
import com.fernt0411.myproduct.data.d_usecase.dashboard.*
import com.fernt0411.myproduct.data.d_usecase.security.*
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCase
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCaseImpl
import com.fernt0411.myproduct.data.d_usecase.user.SaveUserLocalUseCase
import com.fernt0411.myproduct.data.d_usecase.user.SaveUserLocalUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class UseCaseModule {


    //Auth
    @Binds
    abstract fun bindLoginUseCaseImpl(getLoginUseCaseImpl: LoginUseCaseImpl): LoginUseCase


    //User
    @Binds
    abstract fun bindSaveUserLocalUseCaseImpl(saveUserLocalUseCaseImpl: SaveUserLocalUseCaseImpl): SaveUserLocalUseCase

    @Binds
    abstract fun bindGetUserLocalUseCaseImpl(getUserLocalUseCaseImpl: GetUserLocalUseCaseImpl): GetUserLocalUseCase

    //Security
    @Binds
    abstract fun bindSaveTokenLocalUseCaseImpl(saveTokenLocalUseCaseImpl: SaveTokenLocalUseCaseImpl): SaveTokenLocalUseCase

    @Binds
    abstract fun bindGetTokenLocalUseCaseImpl(getTokenLocalUseCaseImpl: GetTokenLocalUseCaseImpl): GetTokenLocalUseCase

    @Binds
    abstract fun bindResetLocalUseCaseImpl(resetLocalUseCaseImpl: ResetLocalUseCaseImpl): ResetLocalUseCase


    //Dashboard
    @Binds
    abstract fun bindGetResumeUseCaseImpl(getResumeUseCaseImpl: GetResumeUseCaseImpl): GetResumeUseCase

    @Binds
    abstract fun bindGetExchangeSimulationUseCaseImpl(getExchangeSimulationUseCaseImpl: GetExchangeSimulationUseCaseImpl): GetExchangeSimulationUseCase

    @Binds
    abstract fun bindGetHistoryUseCaseCaseImpl(getHistoryUseCaseImpl: GetHistoryUseCaseImpl): GetHistoryUseCase

    @Binds
    abstract fun bindGetSimulationCreditUseCaseCaseImpl(getSimulationCreditUseCaseImpl: GetSimulationCreditUseCaseImpl): GetSimulationCreditUseCase

    @Binds
    abstract fun bindGetCardSettingsUseCaseImpl(getCardSettingsUseCaseImpl: GetCardSettingsUseCaseImpl): GetCardSettingsUseCase

    @Binds
    abstract fun bindUpdateInternetSettingStateUseCaseImpl(UpdateInternetSettingStateUseCaseImpl: UpdateInternetSettingStateUseCaseImpl): UpdateInternetSettingStateUseCase

    @Binds
    abstract fun bindUpdateForeignSettingStateUseCaseImpl(updateForeignSettingStateUseCaseImpl: UpdateForeignSettingStateUseCaseImpl): UpdateForeignSettingStateUseCase


    @Binds
    abstract fun bindGetInquiryUseCaseImpl(getInquiryUseCaseImpl: GetInquiryUseCaseImpl): GetInquiryUseCase


    @Binds
    abstract fun bindSaveInquiryUseCaseImpl(saveInquiryUseCaseImpl: SaveInquiryUseCaseImpl): SaveInquiryUseCase


    @Binds
    abstract fun bindGetPublicationsUseCaseImpl(getPublicationsUseCaseImpl: GetPublicationsUseCaseImpl): GetPublicationsUseCase


    @Binds
    abstract fun bindCreatePublicationUseCaseImpl(createPublicationUseCaseImpl: CreatePublicationUseCaseImpl): CreatePublicationUseCase


}