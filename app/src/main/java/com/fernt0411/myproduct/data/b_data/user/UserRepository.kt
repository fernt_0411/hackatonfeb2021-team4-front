package com.fernt0411.myproduct.data.b_data.user


import com.fernt0411.myproduct.data.c_domain.model.User


interface UserRepository {
     fun saveUserLocal(user: User)
     fun getUserLocal(): User
}