package com.fernt0411.myproduct.data.d_usecase.user

import com.fernt0411.myproduct.data.b_data.user.UserRepository
import com.fernt0411.myproduct.data.c_domain.model.User
import javax.inject.Inject

class SaveUserLocalUseCaseImpl @Inject constructor(
    private val userRepository: UserRepository

) : SaveUserLocalUseCase {
    override  fun invoke(user: User) =
        userRepository.saveUserLocal(user)
}

interface SaveUserLocalUseCase {
     operator fun invoke(user: User)
}