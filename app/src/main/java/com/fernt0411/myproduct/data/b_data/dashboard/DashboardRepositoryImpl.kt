package com.fernt0411.myproduct.data.b_data.dashboard


import com.fernt0411.myproduct.data.a_framework.dashboard.DashboardNetwork
import com.fernt0411.myproduct.data.c_domain.model.*
import com.fernt0411.myproduct.data.c_domain.req.CreatePublicationReq
import com.fernt0411.myproduct.data.c_domain.res.StatusOkRes
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class DashboardRepositoryImpl @Inject constructor(
    private val dashboardNetwork: DashboardNetwork
) : DashboardRepository {
    override suspend fun getResume(): Resource<Resume> {
        return dashboardNetwork.getResume()
    }

    override suspend fun getExchangeSimulation(simulationPetition: SimulationPetition): Resource<Simulation> {
        return dashboardNetwork.getExchangeSimulation(simulationPetition)
    }


    override suspend fun getHistory(): Resource<History> {
        return dashboardNetwork.getHistory()
    }

    override suspend fun getSimulationCredit(creditPetition: CreditPetition): Resource<SimulationCredit> {
        return dashboardNetwork.getSimulationCredit(creditPetition)
    }

    override suspend fun getCardSettings(): Resource<Card> {
        return dashboardNetwork.getCardSettings()
    }


    override suspend fun updateInternetSettingState(isActive: Int): Resource<StatusOk> {
        return dashboardNetwork.updateInternetSettingState(isActive)
    }

    override suspend fun updateForeignSettingState(isActive: Int): Resource<StatusOk> {
        return dashboardNetwork.updateForeignSettingState(isActive)
    }

    override suspend fun getInquiry(): Resource<Inquiry>{
        return dashboardNetwork.getInquiry()
    }
    override suspend fun saveInquiry(questionsResolved: Map<String, Int>): Resource<StatusOk>{
        return dashboardNetwork.saveInquiry(questionsResolved)
    }


    override suspend fun getPublications(): Resource<ArrayList<Publication>> {
        return dashboardNetwork.getPublications()
    }

    override suspend fun createPublication(createPublicationReq: CreatePublicationReq): Resource<CreatePublicationReq> {
        return dashboardNetwork.createPublication(createPublicationReq)
    }
}