package com.fernt0411.myproduct.data.a_framework.dashboard

import com.fernt0411.myproduct.data.c_domain.converter.toData
import com.fernt0411.myproduct.data.c_domain.converter.toDomain
import com.fernt0411.myproduct.data.c_domain.model.*
import com.fernt0411.myproduct.data.c_domain.req.CreatePublicationReq
import com.fernt0411.myproduct.data.c_domain.req.CreditPetitionReq
import com.fernt0411.myproduct.data.c_domain.req.SimulationPetitionReq
import com.fernt0411.myproduct.data.c_domain.res.*
import com.fernt0411.myproduct.utils.response.NetworkResponse
import com.fernt0411.myproduct.utils.response.Resource
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class DashboardNetworkImpl @Inject constructor(
    private val dashboardApi: DashboardApi,
    private val gson: Gson

) : DashboardNetwork, NetworkResponse() {
    override suspend fun getResume(): Resource<Resume> {

        val result = getResult { dashboardApi.getResume() }
        val dataRes = gson.fromJson(gson.toJson(result.data), ResumeRes::class.java)
        return Resource.success(dataRes.toDomain())
    }

    override suspend fun getExchangeSimulation(simulationPetition: SimulationPetition): Resource<Simulation> {

        val dataReq = gson.fromJson(
            gson.toJson(simulationPetition.toData()),
            SimulationPetitionReq::class.java
        )

        val result = getResult { dashboardApi.getExchangeSimulation(dataReq) }
        val dataRes = gson.fromJson(gson.toJson(result.data), SimulationRes::class.java)

        return Resource.success(dataRes.toDomain())

    }


    override suspend fun getHistory(): Resource<History> {
        val result = getResult { dashboardApi.getHistory() }
        val dataRes = gson.fromJson(gson.toJson(result.data), HistoryRes::class.java)
        return Resource.success(dataRes.toDomain())
    }

    override suspend fun getSimulationCredit(creditPetition: CreditPetition): Resource<SimulationCredit> {

        val dataReq = gson.fromJson(
            gson.toJson(creditPetition.toData()),
            CreditPetitionReq::class.java
        )

        val result = getResult { dashboardApi.getSimulationCredit(dataReq) }
        val dataRes = gson.fromJson(gson.toJson(result.data), SimulationCreditRes::class.java)

        return Resource.success(dataRes.toDomain())
    }

    override suspend fun getCardSettings(): Resource<Card> {
        val result = getResult { dashboardApi.getCardSettings() }
        val dataRes = gson.fromJson(gson.toJson(result.data), CardRes::class.java)
        return Resource.success(dataRes.toDomain())

    }

    override suspend fun updateInternetSettingState(isActive: Int): Resource<StatusOk> {
        val result = getResult { dashboardApi.updateInternetSettingState(isActive) }
        val dataRes = gson.fromJson(gson.toJson(result.data), StatusOkRes::class.java)
        return Resource.success(dataRes.toDomain())

    }

    override suspend fun updateForeignSettingState(isActive: Int): Resource<StatusOk> {
        val result = getResult { dashboardApi.updateForeignSettingState(isActive) }
        val dataRes = gson.fromJson(gson.toJson(result.data), StatusOkRes::class.java)
        return Resource.success(dataRes.toDomain())
    }


    override suspend fun getInquiry(): Resource<Inquiry> {
        val result = getResult { dashboardApi.getInquiry() }
        val dataRes = gson.fromJson(gson.toJson(result.data), InquiryRes::class.java)
        return Resource.success(dataRes.toDomain())
    }

    override suspend fun saveInquiry(questionsResolved: Map<String, Int>): Resource<StatusOk> {
        val result = getResult { dashboardApi.saveInquiry(questionsResolved) }
        val dataRes = gson.fromJson(gson.toJson(result.data), StatusOkRes::class.java)
        return Resource.success(dataRes.toDomain())
    }


    override suspend fun getPublications(): Resource<ArrayList<Publication>> {
        val result = getResult { dashboardApi.getPublications() }
        return Resource.success(result.data)
    }

    override suspend fun createPublication(createPublicationReq: CreatePublicationReq): Resource<CreatePublicationReq> {
        val result = getResult { dashboardApi.createPublication(createPublicationReq) }
        return Resource.success(result.data)
    }
}
