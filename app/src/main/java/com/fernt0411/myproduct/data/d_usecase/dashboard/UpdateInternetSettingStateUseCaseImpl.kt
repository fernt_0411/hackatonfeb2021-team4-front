package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.StatusOk
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class UpdateInternetSettingStateUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : UpdateInternetSettingStateUseCase {
    override suspend fun invoke(isActive: Int): Resource<StatusOk> =
        dashboardRepository.updateInternetSettingState(isActive)
}

interface UpdateInternetSettingStateUseCase {
    suspend operator fun invoke(isActive: Int): Resource<StatusOk>
}