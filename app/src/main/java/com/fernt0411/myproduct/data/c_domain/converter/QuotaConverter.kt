package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Quota
import com.fernt0411.myproduct.data.c_domain.res.QuotaRes

fun QuotaRes.toDomain(): Quota {
    return Quota().also { domain ->

        domain.amount = this.amount
        domain.month = this.month


    }
}

fun Quota.toData(): QuotaRes {
    return QuotaRes().also { data ->
        data.amount = this.amount
        data.month = this.month


    }
}