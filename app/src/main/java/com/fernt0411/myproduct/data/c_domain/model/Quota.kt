package com.fernt0411.myproduct.data.c_domain.model

data class Quota(

    var month: String = String(),
    var amount: Double = 0.0,

    )