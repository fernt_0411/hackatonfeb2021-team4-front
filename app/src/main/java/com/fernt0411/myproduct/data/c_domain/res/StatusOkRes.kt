package com.fernt0411.myproduct.data.c_domain.res

data class StatusOkRes (
    var code: Int = 0,
    var message: String = String()
)

