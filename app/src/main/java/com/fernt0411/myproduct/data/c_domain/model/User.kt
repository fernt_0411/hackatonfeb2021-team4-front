package com.fernt0411.myproduct.data.c_domain.model

data class User(
    var first_name: String = String(),
    var last_name: String = String(),
    var doc_type: String = String(),
    var doc_nro: String = String(),
    var email: String = String(),

    )
