package com.fernt0411.myproduct.data.c_domain.model

data class CreditPetition(
    var quota: Int = 0,
    var amount: Double = 0.0,
)
