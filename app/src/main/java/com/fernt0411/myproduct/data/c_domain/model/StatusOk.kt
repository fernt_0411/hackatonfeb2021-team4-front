package com.fernt0411.myproduct.data.c_domain.model

data class StatusOk (
    var code: Int = 0,
    var message: String = String()
)

