package com.fernt0411.myproduct.data.c_domain.res


data class CardSettingsRes(

    var card_foreign: Int = 0,
    var card_internet: Int = 0
)