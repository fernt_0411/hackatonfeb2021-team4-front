package com.fernt0411.myproduct.data.b_data.security


import com.fernt0411.myproduct.data.a_framework.security.SecurityLocal
import com.fernt0411.myproduct.data.a_framework.user.UserLocal
import com.fernt0411.myproduct.data.c_domain.model.User
import javax.inject.Inject

class SecurityRepositoryImpl @Inject constructor(
    private val securityLocal: SecurityLocal
) : SecurityRepository {
    override fun saveTokenLocal(token: String) {
        return securityLocal.saveTokenLocal(token)
    }

    override fun getTokenLocal(): String {
        return securityLocal.getTokenLocal()
    }

    override fun resetLocal(){
        return securityLocal.resetLocal()
    }

}