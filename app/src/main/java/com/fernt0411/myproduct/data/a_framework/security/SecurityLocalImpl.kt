package com.fernt0411.myproduct.data.a_framework.security


import android.content.SharedPreferences
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class SecurityLocalImpl @Inject constructor(

    private val sharedPreferences: SharedPreferences

) : SecurityLocal {
    override fun saveTokenLocal(token: String) {

        with(sharedPreferences.edit()) {
            putString("token", token)
            commit()
        }

    }


    override fun getTokenLocal(): String {
        return sharedPreferences.getString("token", String()).toString()

    }

    override fun resetLocal() {
        sharedPreferences.edit().clear().commit();
    }

}
