package com.fernt0411.myproduct.data.c_domain.model

data class Publication(

    var date: String = String(),
    var name: String = String(),
    var lastname: String = String(),
    var description: String = String(),
    var type: String = String(),
    var local: String = String(),
    var address: String = String(),
    var latitude: Double = 0.0,
    var longitude: Double = 0.0


)