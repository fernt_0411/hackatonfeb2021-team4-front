package com.fernt0411.myproduct.data.b_data.dashboard


import com.fernt0411.myproduct.data.c_domain.model.*
import com.fernt0411.myproduct.data.c_domain.req.CreatePublicationReq
import com.fernt0411.myproduct.data.c_domain.res.StatusOkRes
import com.fernt0411.myproduct.utils.response.Resource


interface DashboardRepository {
    suspend fun getResume(): Resource<Resume>
    suspend fun getExchangeSimulation(simulationPetition: SimulationPetition): Resource<Simulation>
    suspend fun getHistory(): Resource<History>
    suspend fun getSimulationCredit(creditPetition: CreditPetition): Resource<SimulationCredit>
    suspend fun getCardSettings(): Resource<Card>


    suspend fun updateInternetSettingState(isActive: Int): Resource<StatusOk>
    suspend fun updateForeignSettingState(isActive: Int): Resource<StatusOk>

    suspend fun getInquiry(): Resource<Inquiry>
    suspend fun saveInquiry(questionsResolved: Map<String, Int>): Resource<StatusOk>


    suspend fun getPublications(): Resource<ArrayList<Publication>>

    suspend fun createPublication(createPublicationReq: CreatePublicationReq): Resource<CreatePublicationReq>
}