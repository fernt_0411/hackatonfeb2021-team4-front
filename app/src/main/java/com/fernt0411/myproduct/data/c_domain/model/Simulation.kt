package com.fernt0411.myproduct.data.c_domain.model

data class Simulation(
    var exchange: String = String(),
    var simulationAmount: Double = 0.0,
)
