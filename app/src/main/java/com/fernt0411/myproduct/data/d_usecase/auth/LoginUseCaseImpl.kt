package com.fernt0411.myproduct.data.d_usecase.auth

import com.fernt0411.myproduct.data.b_data.auth.AuthRepository
import com.fernt0411.myproduct.data.b_data.security.SecurityRepository
import com.fernt0411.myproduct.data.b_data.user.UserRepository
import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class LoginUseCaseImpl @Inject constructor(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository,
    private val securityRepository: SecurityRepository

) : LoginUseCase {
    override suspend fun invoke(dni: String, password: String): Resource<Auth> {
        val result = authRepository.authLogin(dni, password)
       // result.data?.user?.let { userRepository.saveUserLocal(it) }
       // result.data?.token?.let { securityRepository.saveTokenLocal(it) }
        return result
    }

}

interface LoginUseCase {
    suspend operator fun invoke(dni: String, password: String): Resource<Auth>
}