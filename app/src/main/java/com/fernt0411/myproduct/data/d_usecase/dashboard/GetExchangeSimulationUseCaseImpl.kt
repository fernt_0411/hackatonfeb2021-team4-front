package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.Resume
import com.fernt0411.myproduct.data.c_domain.model.Simulation
import com.fernt0411.myproduct.data.c_domain.model.SimulationPetition
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject


class GetExchangeSimulationUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : GetExchangeSimulationUseCase {
    override suspend fun invoke(simulationPetition: SimulationPetition): Resource<Simulation> =
        dashboardRepository.getExchangeSimulation(simulationPetition)
}

interface GetExchangeSimulationUseCase {
    suspend operator fun invoke(simulationPetition: SimulationPetition): Resource<Simulation>
}


