package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.Resume
import com.fernt0411.myproduct.utils.response.Resource

import javax.inject.Inject

class GetResumeUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : GetResumeUseCase {
    override suspend fun invoke(): Resource<Resume> =
        dashboardRepository.getResume()
}

interface GetResumeUseCase {
    suspend operator fun invoke(): Resource<Resume>
}