package com.fernt0411.myproduct.data.b_data.user


import com.fernt0411.myproduct.data.c_domain.res.AuthRes
import com.fernt0411.myproduct.data.a_framework.auth.AuthNetwork
import com.fernt0411.myproduct.data.a_framework.user.UserLocal
import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val userLocal: UserLocal
) : UserRepository {
    override  fun saveUserLocal(user: User) {
        return userLocal.saveUserLocal(user)
    }

    override  fun getUserLocal(): User {
        return userLocal.getUserLocal()
    }


}