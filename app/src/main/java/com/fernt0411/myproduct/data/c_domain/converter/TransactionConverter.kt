package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Publication
import com.fernt0411.myproduct.data.c_domain.res.PublicationRes

fun PublicationRes.toDomain(): Publication {
    return Publication().also { domain ->

        domain.date = this.date
        domain.name = this.names
        domain.lastname = this.lastname
        domain.description = this.description
       // domain.type = this.type
        domain.local = this.local
        domain.address = this.address
        domain.latitude = this.latitude
        domain.longitude = this.longitude


    }
}

fun Publication.toData(): PublicationRes {
    return PublicationRes().also { data ->


    }
}