package com.fernt0411.myproduct.data.c_domain.res

data class HistoryRes(

    var code: String = String(),
    var history: MutableMap<String, PublicationRes> = mutableMapOf()
)