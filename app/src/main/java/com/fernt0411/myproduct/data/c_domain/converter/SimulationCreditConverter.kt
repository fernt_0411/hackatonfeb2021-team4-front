package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.SimulationCredit
import com.fernt0411.myproduct.data.c_domain.res.SimulationCreditRes

fun SimulationCreditRes.toDomain(): SimulationCredit {
    return SimulationCredit().also { domain ->
        domain.loan = this.loan
        domain.message = this.message

        this.schedule.forEach {

            domain.schedule.add(it.toDomain())
        }

    }
}

fun SimulationCredit.toData(): SimulationCreditRes {
    return SimulationCreditRes().also { data ->
        data.loan = this.loan
        data.message = this.message

        this.schedule.forEach {

            data.schedule.add(it.toData())
        }


    }
}