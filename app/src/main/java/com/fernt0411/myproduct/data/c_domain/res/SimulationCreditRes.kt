package com.fernt0411.myproduct.data.c_domain.res

data class SimulationCreditRes(
    var code: Int = 0,
    var loan: Boolean = true,
    var message: String = String(),
    var schedule: ArrayList<QuotaRes> = ArrayList()
)


