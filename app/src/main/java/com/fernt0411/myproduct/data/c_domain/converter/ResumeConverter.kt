package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Resume
import com.fernt0411.myproduct.data.c_domain.res.ResumeRes


fun ResumeRes.toDomain(): Resume {


    return Resume().also { domain ->

        this.mes.forEach {

            var month = it.value.toDomain()
            month.monthDescription = it.key
            domain.months.add(month)

        }


    }
}

fun Resume.toData(): ResumeRes {
    return ResumeRes().also { data ->


    }
}