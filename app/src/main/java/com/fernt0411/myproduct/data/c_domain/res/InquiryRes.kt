package com.fernt0411.myproduct.data.c_domain.res

data class InquiryRes(
    var code: Int = 0,
    var inquiry: InquiryQuestionsRes = InquiryQuestionsRes()

)