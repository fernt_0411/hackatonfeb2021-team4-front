package com.fernt0411.myproduct.data.c_domain.model

data class ModuleOption(
    var id: Int = 0,
    var image: Int = 0,
    var title: String = String(),
    var description: String = String(),
    var color: String = String(),
    var category: String =  String(),

)
