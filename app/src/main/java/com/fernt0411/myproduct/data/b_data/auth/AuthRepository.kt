package com.fernt0411.myproduct.data.b_data.auth


import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.data.c_domain.res.AuthRes
import com.fernt0411.myproduct.utils.response.Resource


interface AuthRepository {
    suspend fun authLogin(dni: String, password: String): Resource<Auth>
}