package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.History
import com.fernt0411.myproduct.data.c_domain.res.HistoryRes


fun HistoryRes.toDomain(): History {


    return History().also { domain ->

        this.history.forEach {

            var transaction = it.value.toDomain()


            domain.history.add(transaction)


        }

    }
}

fun History.toData(): HistoryRes {
    return HistoryRes().also { data ->


    }
}