package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.InquiryQuestions
import com.fernt0411.myproduct.data.c_domain.res.InquiryQuestionsRes


fun InquiryQuestionsRes.toDomain(): InquiryQuestions {

    return InquiryQuestions().also { domain ->
        this.questions.forEach {
            domain.questions[it.key] = it.value
        }
        domain.status = this.status

    }
}

fun InquiryQuestions.toData(): InquiryQuestionsRes {
    return InquiryQuestionsRes().also { data ->

        this.questions.forEach {
            data.questions[it.key] = it.value
        }

        data.status = this.status

    }
}