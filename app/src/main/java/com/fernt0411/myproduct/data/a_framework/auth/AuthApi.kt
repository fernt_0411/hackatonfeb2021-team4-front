package com.fernt0411.myproduct.data.a_framework.auth

import com.fernt0411.myproduct.data.c_domain.res.AuthRes
import com.fernt0411.myproduct.utils.constant.ApiConstants.AUTH
import com.fernt0411.myproduct.utils.di.AppModule
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface AuthApi {
    @Headers("Content-Type: application/x-www-form-urlencoded")

    @FormUrlEncoded
    @POST(AUTH)

    suspend fun authLogin(
        @Field("user") user: String,
        @Field("pass") pass: String
    ): Response<AuthRes>
}