package com.fernt0411.myproduct.data.c_domain.res

data class InquiryQuestionsRes(

    var questions: MutableMap<String, String> = mutableMapOf(),
    var status: Boolean = false

)