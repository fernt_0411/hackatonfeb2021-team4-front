package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.History
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class GetHistoryUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : GetHistoryUseCase {
    override suspend fun invoke(): Resource<History> =
        dashboardRepository.getHistory()
}

interface GetHistoryUseCase {
    suspend operator fun invoke(): Resource<History>
}