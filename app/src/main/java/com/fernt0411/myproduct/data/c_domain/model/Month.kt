package com.fernt0411.myproduct.data.c_domain.model


class Month(

    var income: Int = 0,
    var expenses: Int = 0,
    var credits: Int = 0,
    var loans: Int = 0,
    var assets: Int = 0,
    var monthDescription: String = String()

)