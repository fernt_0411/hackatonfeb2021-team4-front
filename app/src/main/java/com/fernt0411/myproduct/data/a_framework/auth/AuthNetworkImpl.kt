package com.fernt0411.myproduct.data.a_framework.auth

import com.fernt0411.myproduct.data.c_domain.converter.toDomain
import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.data.c_domain.res.AuthRes
import com.fernt0411.myproduct.utils.di.AppModule
import com.fernt0411.myproduct.utils.response.NetworkResponse
import com.fernt0411.myproduct.utils.response.Resource
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class AuthNetworkImpl @Inject constructor(
    private val authApi: AuthApi,
    private val gson: Gson

) : AuthNetwork, NetworkResponse() {
    override suspend fun authLogin(dni: String, password: String): Resource<Auth> {

        val result = getResult { authApi.authLogin(dni, password) }
        val dataRes = gson.fromJson(gson.toJson(result.data), AuthRes::class.java)
        return Resource.success(dataRes.toDomain())
    }
}
