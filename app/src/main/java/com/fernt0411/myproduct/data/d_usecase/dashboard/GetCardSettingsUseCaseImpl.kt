package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.Card
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class GetCardSettingsUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : GetCardSettingsUseCase {
    override suspend fun invoke(): Resource<Card> =
        dashboardRepository.getCardSettings()
}

interface GetCardSettingsUseCase {
    suspend operator fun invoke(): Resource<Card>
}