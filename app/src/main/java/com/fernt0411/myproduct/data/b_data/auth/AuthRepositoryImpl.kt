package com.fernt0411.myproduct.data.b_data.auth


import com.fernt0411.myproduct.data.c_domain.res.AuthRes
import com.fernt0411.myproduct.data.a_framework.auth.AuthNetwork
import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val authNetwork: AuthNetwork
) : AuthRepository {
    override suspend fun authLogin(dni: String, password: String): Resource<Auth> {
        return authNetwork.authLogin(dni, password)
    }


}