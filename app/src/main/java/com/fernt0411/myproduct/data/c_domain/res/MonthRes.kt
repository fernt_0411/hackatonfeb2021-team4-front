package com.fernt0411.myproduct.data.c_domain.res

data class MonthRes(

    var ingresos: Int = 0,
    var egresos: Int = 0,
    var creditos: Int = 0,
    var prestamos: Int = 0,
    var haberes: Int = 0

)