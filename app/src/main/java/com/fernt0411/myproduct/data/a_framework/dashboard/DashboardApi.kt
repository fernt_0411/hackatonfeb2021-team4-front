package com.fernt0411.myproduct.data.a_framework.dashboard

import com.fernt0411.myproduct.data.c_domain.model.Publication
import com.fernt0411.myproduct.data.c_domain.req.CreatePublicationReq
import com.fernt0411.myproduct.data.c_domain.req.CreditPetitionReq
import com.fernt0411.myproduct.data.c_domain.req.SimulationPetitionReq
import com.fernt0411.myproduct.data.c_domain.res.*
import com.fernt0411.myproduct.utils.constant.ApiConstants.CARD
import com.fernt0411.myproduct.utils.constant.ApiConstants.CREATE_PUBLICATION
import com.fernt0411.myproduct.utils.constant.ApiConstants.EXCHANGE
import com.fernt0411.myproduct.utils.constant.ApiConstants.HISTORY
import com.fernt0411.myproduct.utils.constant.ApiConstants.INQUIRY
import com.fernt0411.myproduct.utils.constant.ApiConstants.LOAN
import com.fernt0411.myproduct.utils.constant.ApiConstants.PUBLICATION
import com.fernt0411.myproduct.utils.constant.ApiConstants.RESUME
import retrofit2.Response
import retrofit2.http.*


interface DashboardApi {

    @POST(RESUME)
    suspend fun getResume(): Response<ResumeRes>

    @POST(EXCHANGE)
    suspend fun getExchangeSimulation(@Body simulationPetitonReq: SimulationPetitionReq): Response<SimulationRes>

    @POST(HISTORY)
    suspend fun getHistory(): Response<HistoryRes>

    @POST(LOAN)
    suspend fun getSimulationCredit(@Body creditPetitionReq: CreditPetitionReq): Response<SimulationCreditRes>

    @GET(CARD)
    suspend fun getCardSettings(): Response<CardRes>


    @POST(CARD)
    suspend fun updateInternetSettingState(@Body isActive: Int): Response<CardRes>

    @POST(CARD)
    suspend fun updateForeignSettingState(@Body isActive: Int): Response<CardRes>


    @GET(INQUIRY)
    suspend fun getInquiry(): Response<InquiryRes>

    @POST(INQUIRY)
    @FormUrlEncoded
    suspend fun saveInquiry(@FieldMap questionsResolved: Map<String, Int>): Response<StatusOkRes>


    @GET(PUBLICATION)
    suspend fun getPublications(): Response<ArrayList<Publication>>

    @POST(CREATE_PUBLICATION)
    suspend fun createPublication(@Body createPublicationReq: CreatePublicationReq): Response<CreatePublicationReq>


}