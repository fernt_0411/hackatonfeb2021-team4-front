package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Card
import com.fernt0411.myproduct.data.c_domain.res.CardRes

fun CardRes.toDomain(): Card {
    return Card().also { domain ->
        domain.cardSettings = this.card.toDomain()


    }
}

fun Card.toData(): CardRes {
    return CardRes().also { data ->

        data.card = this.cardSettings.toData()
    }
}