package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.data.c_domain.res.AuthRes

fun AuthRes.toDomain(): Auth {
    return Auth().also { domain ->

        domain.code = this.code
        domain.user = this.user
        domain.token = this.token


    }
}

fun Auth.toData(): AuthRes {
    return AuthRes().also { data ->
        data.code = this.code
        data.user = this.user
        data.token = this.token
    }
}