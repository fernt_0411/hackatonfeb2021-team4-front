package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.StatusOk
import com.fernt0411.myproduct.data.c_domain.res.StatusOkRes

fun StatusOkRes.toDomain(): StatusOk {
    return StatusOk().also { domain ->

        domain.code = this.code
        domain.message = this.message


    }
}

fun StatusOk.toData(): StatusOkRes {
    return StatusOkRes().also { data ->

        data.code = this.code
        data.message = this.message


    }
}