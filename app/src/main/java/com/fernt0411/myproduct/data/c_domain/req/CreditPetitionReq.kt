package com.fernt0411.myproduct.data.c_domain.req

data class CreditPetitionReq(
    var quota: Int = 0,
    var amount: Double = 0.0,
)
