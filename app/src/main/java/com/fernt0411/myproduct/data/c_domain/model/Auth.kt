package com.fernt0411.myproduct.data.c_domain.model

data class Auth(
    var code: String = String(),
    var user: User = User(),
    var token: String = String(),
)
