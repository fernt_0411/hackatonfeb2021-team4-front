package com.fernt0411.myproduct.data.c_domain.model

data class SimulationCredit(
    var code: Int = 0,
    var loan: Boolean = true,
    var message: String = String(),
    var schedule: ArrayList<Quota> = ArrayList()
)


