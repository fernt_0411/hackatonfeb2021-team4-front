package com.fernt0411.myproduct.data.c_domain.res

data class SimulationRes(
    var code: Int=0,
    var exchange: String = String(),
    var total: Double = 0.0,

)
