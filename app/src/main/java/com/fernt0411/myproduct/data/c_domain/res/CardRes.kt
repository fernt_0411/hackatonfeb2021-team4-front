package com.fernt0411.myproduct.data.c_domain.res

data class CardRes(
    var code: String = String(),
    var card: CardSettingsRes = CardSettingsRes()
)