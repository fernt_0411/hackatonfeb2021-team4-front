package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.StatusOk
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class SaveInquiryUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : SaveInquiryUseCase {
    override suspend fun invoke(questionsResolved: Map<String, Int>): Resource<StatusOk> =
        dashboardRepository.saveInquiry(questionsResolved)
}

interface SaveInquiryUseCase {
    suspend operator fun invoke(questionsResolved: Map<String, Int>): Resource<StatusOk>
}