package com.fernt0411.myproduct.data.c_domain.model

data class SimulationPetition(
    var sell: Int = 0,
    var amount: Double = 0.0,
    )
