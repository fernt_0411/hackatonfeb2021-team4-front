package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Inquiry
import com.fernt0411.myproduct.data.c_domain.res.InquiryRes

fun InquiryRes.toDomain(): Inquiry {
    return Inquiry().also { domain ->
        domain.inquiryQuestions = this.inquiry.toDomain()



    }
}

fun Inquiry.toData(): InquiryRes {
    return InquiryRes().also { data ->

        data.inquiry = this.inquiryQuestions.toData()
    }
}