package com.fernt0411.myproduct.data.c_domain.model

data class InquiryQuestions(

    var questions: MutableMap<String, String> = mutableMapOf(),
    var status: Boolean = false

)