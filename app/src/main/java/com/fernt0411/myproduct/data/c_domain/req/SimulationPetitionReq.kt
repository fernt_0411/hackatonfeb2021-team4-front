package com.fernt0411.myproduct.data.c_domain.req

data class SimulationPetitionReq(
    var sell: Int = 0,
    var amount: Double = 0.0,
    )
