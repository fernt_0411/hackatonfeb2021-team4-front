package com.fernt0411.myproduct.data.c_domain.model

enum class TypePublication(val key: String) {
    DONACION("D"),
    RECEPCION("R"),

}
