package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Month
import com.fernt0411.myproduct.data.c_domain.model.Simulation
import com.fernt0411.myproduct.data.c_domain.res.MonthRes
import com.fernt0411.myproduct.data.c_domain.res.SimulationRes


fun SimulationRes.toDomain(): Simulation {
    return Simulation().also { domain ->

        domain.exchange = this.exchange
        domain.simulationAmount = this.total



    }
}

fun Simulation.toData(): SimulationRes {
    return SimulationRes().also { data ->

        data.exchange = this.exchange
        data.total = this.simulationAmount

    }
}


