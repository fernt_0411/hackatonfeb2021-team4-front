package com.fernt0411.myproduct.data.a_framework.security

import com.fernt0411.myproduct.data.c_domain.model.User

interface SecurityLocal {

    fun saveTokenLocal(token: String)
    fun getTokenLocal(): String
    fun resetLocal()
}