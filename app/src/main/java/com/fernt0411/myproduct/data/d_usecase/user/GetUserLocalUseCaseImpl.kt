package com.fernt0411.myproduct.data.d_usecase.user

import com.fernt0411.myproduct.data.b_data.user.UserRepository
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class GetUserLocalUseCaseImpl @Inject constructor(
    private val userRepository: UserRepository

) : GetUserLocalUseCase {
    override fun invoke(): User =
        userRepository.getUserLocal()
}

interface GetUserLocalUseCase {
    operator fun invoke(): User
}