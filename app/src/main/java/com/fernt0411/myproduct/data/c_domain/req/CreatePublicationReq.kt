package com.fernt0411.myproduct.data.c_domain.req

data class CreatePublicationReq(
    var id: String = String(),
    var name: String = String(),
    var lastname: String = String(),
    var description: String = String(),
    var type: String = String(),
    var localname: String = String(),
    var localaddress: String = String(),
    var latitude: Double = 0.0,
    var longitude: Double = 0.0
)
