package com.fernt0411.myproduct.data.c_domain.model

enum class TypeCurrency(val key: String, val f: String) {
    PEN("soles", "S/"),
    USD("dolares", "$"),

}
