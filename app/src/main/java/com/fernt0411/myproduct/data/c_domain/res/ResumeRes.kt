package com.fernt0411.myproduct.data.c_domain.res

import com.fernt0411.myproduct.data.c_domain.model.User

data class ResumeRes(

    var code: String = String(),
    var mes: MutableMap<String, MonthRes> = mutableMapOf()
)