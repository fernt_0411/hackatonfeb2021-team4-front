package com.fernt0411.myproduct.data.d_usecase.security

import com.fernt0411.myproduct.data.b_data.security.SecurityRepository
import javax.inject.Inject

class ResetLocalUseCaseImpl @Inject constructor(
    private val securityRepository: SecurityRepository

) : ResetLocalUseCase {
    override fun invoke() =
        securityRepository.resetLocal()
}

interface ResetLocalUseCase {
    operator fun invoke()
}