package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.StatusOk
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class UpdateForeignSettingStateUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : UpdateForeignSettingStateUseCase {
    override suspend fun invoke(isActive: Int): Resource<StatusOk> =
        dashboardRepository.updateForeignSettingState(isActive)
}

interface UpdateForeignSettingStateUseCase {
    suspend operator fun invoke(isActive: Int): Resource<StatusOk>
}