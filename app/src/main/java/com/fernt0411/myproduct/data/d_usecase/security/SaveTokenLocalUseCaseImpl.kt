package com.fernt0411.myproduct.data.d_usecase.security


import com.fernt0411.myproduct.data.b_data.security.SecurityRepository
import javax.inject.Inject

class SaveTokenLocalUseCaseImpl @Inject constructor(
    private val securityRepository: SecurityRepository

) : SaveTokenLocalUseCase {
    override fun invoke(token: String) =
        securityRepository.saveTokenLocal(token)
}

interface SaveTokenLocalUseCase {
    operator fun invoke(token: String)
}