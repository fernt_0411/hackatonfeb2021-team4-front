package com.fernt0411.myproduct.data.c_domain.res

import com.fernt0411.myproduct.data.c_domain.model.TypePublication

data class PublicationRes(

    var date: String = String(),
    var names: String = String(),
    var lastname: String = String(),
    var description: String = String(),
    var type: TypePublication = TypePublication.DONACION,
    var local: String = String(),
    var address: String = String(),
    var latitude: Double = 0.0,
    var longitude: Double = 0.0
)