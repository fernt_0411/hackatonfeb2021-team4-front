package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.CreditPetition
import com.fernt0411.myproduct.data.c_domain.model.Month
import com.fernt0411.myproduct.data.c_domain.model.Simulation
import com.fernt0411.myproduct.data.c_domain.model.SimulationPetition
import com.fernt0411.myproduct.data.c_domain.req.CreditPetitionReq
import com.fernt0411.myproduct.data.c_domain.req.SimulationPetitionReq
import com.fernt0411.myproduct.data.c_domain.res.MonthRes
import com.fernt0411.myproduct.data.c_domain.res.SimulationRes


fun CreditPetitionReq.toDomain(): CreditPetition {
    return CreditPetition().also { domain ->

        domain.amount = this.amount
        domain.quota = this.quota


    }
}

fun CreditPetition.toData(): CreditPetitionReq {
    return CreditPetitionReq().also { data ->

        data.amount = this.amount
        data.quota = this.quota

    }
}


