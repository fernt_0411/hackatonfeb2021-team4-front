package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.CreditPetition
import com.fernt0411.myproduct.data.c_domain.model.SimulationCredit
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject


class GetSimulationCreditUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : GetSimulationCreditUseCase {
    override suspend fun invoke(creditPetition: CreditPetition): Resource<SimulationCredit> =
        dashboardRepository.getSimulationCredit(creditPetition)
}

interface GetSimulationCreditUseCase {
    suspend operator fun invoke(creditPetition: CreditPetition): Resource<SimulationCredit>
}

