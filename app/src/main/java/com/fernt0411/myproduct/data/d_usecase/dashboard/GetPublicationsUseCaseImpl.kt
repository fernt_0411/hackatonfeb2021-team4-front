package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.Publication
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class GetPublicationsUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : GetPublicationsUseCase {
    override suspend fun invoke(): Resource<ArrayList<Publication>> =
        dashboardRepository.getPublications()


}

interface GetPublicationsUseCase {
    suspend operator fun invoke(): Resource<ArrayList<Publication>>
}