package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.req.CreatePublicationReq
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class CreatePublicationUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : CreatePublicationUseCase {
    override suspend fun invoke(createPublicationReq: CreatePublicationReq): Resource<CreatePublicationReq> =
         dashboardRepository.createPublication(createPublicationReq)


}

interface CreatePublicationUseCase {
    suspend operator fun invoke(createPublicationReq: CreatePublicationReq): Resource<CreatePublicationReq>
}