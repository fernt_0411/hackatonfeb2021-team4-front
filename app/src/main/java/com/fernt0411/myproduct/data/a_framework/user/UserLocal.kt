package com.fernt0411.myproduct.data.a_framework.user

import com.fernt0411.myproduct.data.c_domain.model.User

interface UserLocal {

     fun saveUserLocal(user: User)
     fun getUserLocal():User
}