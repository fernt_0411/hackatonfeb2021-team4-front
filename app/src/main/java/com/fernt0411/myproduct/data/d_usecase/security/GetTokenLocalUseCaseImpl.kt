package com.fernt0411.myproduct.data.d_usecase.security

import com.fernt0411.myproduct.data.b_data.security.SecurityRepository
import com.fernt0411.myproduct.data.b_data.user.UserRepository
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.utils.response.Resource
import javax.inject.Inject

class GetTokenLocalUseCaseImpl @Inject constructor(
    private val securityRepository: SecurityRepository

) : GetTokenLocalUseCase {
    override fun invoke(): String =
        securityRepository.getTokenLocal()
}

interface GetTokenLocalUseCase {
    operator fun invoke(): String
}