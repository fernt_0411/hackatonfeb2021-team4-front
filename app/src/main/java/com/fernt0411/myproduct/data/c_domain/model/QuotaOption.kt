package com.fernt0411.myproduct.data.c_domain.model

data class QuotaOption(

    var description: String = String(),
    var value: Int = 0,
)