package com.fernt0411.myproduct.data.c_domain.res

import com.fernt0411.myproduct.data.c_domain.model.User

data class AuthRes(
    var code: String = String(),
    var token: String = String(),
    var user: User = User()
)