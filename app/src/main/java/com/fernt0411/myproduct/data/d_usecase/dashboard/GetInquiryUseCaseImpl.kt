package com.fernt0411.myproduct.data.d_usecase.dashboard

import com.fernt0411.myproduct.data.b_data.dashboard.DashboardRepository
import com.fernt0411.myproduct.data.c_domain.model.Inquiry
import com.fernt0411.myproduct.utils.response.Resource

import javax.inject.Inject

class GetInquiryUseCaseImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository

) : GetInquiryUseCase {
    override suspend fun invoke(): Resource<Inquiry> =
        dashboardRepository.getInquiry()
}

interface GetInquiryUseCase {
    suspend operator fun invoke(): Resource<Inquiry>
}