package com.fernt0411.myproduct.data.a_framework.user


import android.content.SharedPreferences
import com.fernt0411.myproduct.data.c_domain.model.User

import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

@ExperimentalCoroutinesApi
class UserLocalImpl @Inject constructor(
    private val gson: Gson,
    private val sharedPreferences: SharedPreferences

) : UserLocal {
    override fun saveUserLocal(user: User) {

        with(sharedPreferences.edit()) {
            putString("user", gson.toJson(user))
            commit()
        }

    }


    override fun getUserLocal(): User {
        val user = sharedPreferences.getString("user", String())
        if (user != null) {
            if (user.isNotEmpty()) {
                return gson.fromJson(sharedPreferences.getString("user", String()), User::class.java)
            }
        }

        return User()

    }

}
