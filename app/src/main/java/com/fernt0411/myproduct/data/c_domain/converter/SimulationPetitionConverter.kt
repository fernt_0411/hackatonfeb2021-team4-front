package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Month
import com.fernt0411.myproduct.data.c_domain.model.Simulation
import com.fernt0411.myproduct.data.c_domain.model.SimulationPetition
import com.fernt0411.myproduct.data.c_domain.req.SimulationPetitionReq
import com.fernt0411.myproduct.data.c_domain.res.MonthRes
import com.fernt0411.myproduct.data.c_domain.res.SimulationRes


fun SimulationPetitionReq.toDomain(): SimulationPetition {
    return SimulationPetition().also { domain ->

        domain.amount = this.amount
        domain.sell = this.sell


    }
}

fun SimulationPetition.toData(): SimulationPetitionReq {
    return SimulationPetitionReq().also { data ->

        data.amount = this.amount
        data.sell = this.sell

    }
}


