package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.CardSettings
import com.fernt0411.myproduct.data.c_domain.res.CardSettingsRes

fun CardSettingsRes.toDomain(): CardSettings {
    return CardSettings().also { domain ->

        domain.cardForeign = this.card_foreign != 0
        domain.cardInternet = this.card_internet != 0


    }
}

fun CardSettings.toData(): CardSettingsRes {
    return CardSettingsRes().also { data ->

        if (this.cardForeign) {
            data.card_foreign = 1
        } else {
            data.card_foreign = 0
        }



        if (this.cardInternet) {
            data.card_internet = 1
        } else {
            data.card_internet = 0
        }


    }
}