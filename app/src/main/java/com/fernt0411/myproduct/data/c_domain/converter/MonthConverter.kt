package com.fernt0411.myproduct.data.c_domain.converter

import com.fernt0411.myproduct.data.c_domain.model.Month
import com.fernt0411.myproduct.data.c_domain.res.MonthRes

fun MonthRes.toDomain(): Month {
    return Month().also { domain ->

        domain.income = this.ingresos
        domain.expenses = this.egresos
        domain.credits = this.creditos
        domain.loans = this.prestamos
        domain.assets = this.haberes



    }
}

fun Month.toData(): MonthRes {
    return MonthRes().also { data ->

        data.ingresos = this.income
        data.egresos = this.expenses
        data.creditos = this.credits
        data.prestamos = this.loans
        data.haberes = this.assets


    }
}