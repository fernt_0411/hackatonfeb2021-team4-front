package com.fernt0411.myproduct.ui.main.home

import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.*
import com.fernt0411.myproduct.databinding.FragmentHomeBinding

import com.fernt0411.myproduct.ui.main.publications.PublicationsAdapter
import com.fernt0411.myproduct.utils.base.BaseFragment
import com.fernt0411.myproduct.utils.response.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>() {

    override val viewModel: HomeViewModel by viewModels()
    override fun getLayoutRes(): Int = R.layout.fragment_home

    private lateinit var adapter: PublicationsAdapter
    private var publications: ArrayList<Publication> = ArrayList()
    private lateinit var moduleOptions: MutableList<ModuleOption>
    val questionResolved: MutableMap<String, Int> = mutableMapOf()
    private lateinit var inquiryDialog: AlertDialog
    private var inquiry: Inquiry = Inquiry()
    private var statusOk: StatusOk = StatusOk()


    override fun initialize() {
        super.initialize()
        initEvents()
        onSubscribeViewModel()
        loadModuleOptions()
        setRecyclerView()

    }

    private fun initEvents() {}


    private fun loadModuleOptions() {


    }

    private fun navigateTo(destiny: Int) {
        Navigation.findNavController(
            this.requireActivity(),
            R.id.navMainHostFragment
        ).navigate(destiny)
    }

    private fun setRecyclerView() {

        publications.add(
            Publication(
                "12//12/21",
                "Fernando",
                "Tupac yupanqui",
                "Hola esta es una decripcion",
                "Recepcion",
                "local",
                "av tupac amaru 2963",
                -12.3434,
                -12.44344
            )
        )
        publications.add(
            Publication(
                "12//12/21",
                "Fernando",
                "Tupac yupanqui",
                "Hola esta es una decripcion",
                "Recepcion",
                "local",
                "av tupac amaru 2963",
                -12.3434,
                -12.44344
            )
        )
        publications.add(
            Publication(
                "12//12/21",
                "Fernando",
                "Tupac yupanqui",
                "Hola esta es una decripcion",
                "Recepcion",
                "local",
                "av tupac amaru 2963",
                -12.3434,
                -12.44344
            )
        )
        publications.add(
            Publication(
                "12//12/21",
                "Fernando",
                "Tupac yupanqui",
                "Hola esta es una decripcion",
                "Recepcion",
                "local",
                "av tupac amaru 2963",
                -12.3434,
                -12.44344
            )
        )

        adapter = PublicationsAdapter()
        adapter.itemClickListener = { _, data ->
            when (data.description) {

            }

        }
        binding.moduleOptionsRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.moduleOptionsRecyclerView.adapter = adapter
        adapter.list = publications

    }


    private fun isValidSendButton(): Boolean {
        var isValid = false

        questionResolved.forEach {
            isValid = it.value != 0
        }

        return isValid
    }

    private fun onSubscribeViewModel() {
        viewModel.getInquiryResult.observe(viewLifecycleOwner, {

            when (it.status) {
                Resource.Status.LOADING -> showFullscreenLoader()
                Resource.Status.SUCCESS -> {

                    inquiry = it.data!!
                    Log.d("miclase", inquiry.toString())


                    hideFullscreenLoader()

                }
                Resource.Status.ERROR -> Log.d("miclase", it.data.toString())
            }

        })


    }


}