package com.fernt0411.myproduct.ui.main.cretepublication


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil

import androidx.lifecycle.Observer
import androidx.navigation.NavController
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.c_domain.req.CreatePublicationReq
import com.fernt0411.myproduct.databinding.ActivityCreatePublicationBinding
import com.fernt0411.myproduct.ui.main.MainActivity
import com.fernt0411.myproduct.ui.main.user.UserActivity
import com.fernt0411.myproduct.utils.base.BaseActivity
import com.fernt0411.myproduct.utils.response.Resource
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class CreatePublicationActivity : BaseActivity() {
    val viewModel: CreatePublicationViewModel by viewModels()
    private lateinit var binding: ActivityCreatePublicationBinding
    private lateinit var navController: NavController
    private lateinit var createPublicationGoButton: MaterialButton
    private lateinit var purchaseChip: Chip
    private lateinit var sellChip: Chip
    private var user: User = User()
    private var type: String = "Donacion"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_publication)

        val toolbar = binding.toolbar
        setSupportActionBar(toolbar)

        setContentView(binding.root)

        initViews()
        initEvents()
        onSubscribeViewModel()
        user = viewModel.onGetUser()


    }

    private fun onSubscribeViewModel() {

        viewModel.createPublicationResult.observe(this, Observer {

            when (it.status) {
                Resource.Status.LOADING -> showFullscreenLoader()
                Resource.Status.SUCCESS -> {


                    Log.d("miclase", it.data.toString())



                    hideFullscreenLoader()

                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }
                Resource.Status.ERROR -> Log.d("miclase", it.data.toString())
            }

        })

    }

    private fun initEvents() {
        var createPublicationReq = CreatePublicationReq()




        createPublicationGoButton.setOnClickListener {
            createPublicationReq.description = viewModel.description.toString()
            createPublicationReq.name = user.first_name
            createPublicationReq.lastname = user.last_name
            createPublicationReq.type = type
            viewModel.onCreatePublication(createPublicationReq)
        }

        purchaseChip.setOnCheckedChangeListener { chip, isChecked ->
            if (isChecked) {
                type = "Donacion"
            }
        }

        sellChip.setOnCheckedChangeListener { chip, isChecked ->
            if (isChecked) {
                type = "Recepcion"
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }


    private fun initViews() {
        createPublicationGoButton = binding.createPublicationGoButton
        purchaseChip = binding.purchaseChip
        sellChip = binding.sellChip


    }

    private fun toUser() {
        val intent = Intent(this, UserActivity::class.java)
        startActivity(intent)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.logout -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun onCreatePublication() {
        finish()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)

    }


}




