package com.fernt0411.myproduct.ui.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.databinding.FragmentSplashBinding
import com.fernt0411.myproduct.utils.base.BaseViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*
import kotlin.concurrent.schedule

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class SplashFragment : Fragment() {
    private lateinit var binding: FragmentSplashBinding
    val viewModel: BaseViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        Timer("customTimer", false).schedule(2000) {
            validateUser()

        }
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }


    private fun validateUser() {
        if (viewModel.isThereUserLocal()) {
            this.findNavController().navigate(R.id.action_splashFragment_to_mainActivity)
        } else {
            this.findNavController().navigate(R.id.action_splashFragment_to_authActivity)
        }

    }


}