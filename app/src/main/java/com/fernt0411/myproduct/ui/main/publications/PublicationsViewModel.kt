package com.fernt0411.myproduct.ui.main.publications

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fernt0411.myproduct.data.c_domain.model.Publication
import com.fernt0411.myproduct.data.c_domain.model.SimulationPetition
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetHistoryUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetPublicationsUseCase
import com.fernt0411.myproduct.utils.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class PublicationsViewModel @ViewModelInject constructor(

    private val getPublicationsUseCase: GetPublicationsUseCase

) : ViewModel() {

    private var simulationPetition: SimulationPetition = SimulationPetition()


    private val _getPublications = MutableLiveData<Resource<ArrayList<Publication>>>()
    val getPublications: LiveData<Resource<ArrayList<Publication>>> get() = _getPublications


    val amount = MutableLiveData("")

    val sell = MutableLiveData(0)

    init {
        onValidateForm(amount = "")
       onGetPublications()
    }

    val isFormValid = MediatorLiveData<Boolean>().apply {
        addSource(amount) {
            value = onValidateForm(amount.value.toString())
            Log.d("user_pass", value.toString())
        }
    }


    fun onGetPublications() {
        viewModelScope.launch {
            _getPublications.value = Resource.loading()
            _getPublications.value = withContext(Dispatchers.IO) {

                getPublicationsUseCase.invoke()
            }
        }
    }


    //crear utils para forms y fields
    private fun onValidateForm(amount: String): Boolean {
        return amount.isNotEmpty()
    }


}







