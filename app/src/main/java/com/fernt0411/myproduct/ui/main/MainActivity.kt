package com.fernt0411.myproduct.ui.main


import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.fernt0411.myproduct.MyProductsApp
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.databinding.ActivityMainBinding
import com.fernt0411.myproduct.ui.main.user.UserActivity
import com.fernt0411.myproduct.utils.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class MainActivity : BaseActivity() {

    val viewModel: MainViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val toolbar = binding.toolbar
        setSupportActionBar(toolbar)

        setContentView(binding.root)
        setUpNavController()

        initViews()


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    private fun setUpNavController() {
        navController = Navigation.findNavController(this, R.id.navMainHostFragment)

    }

    private fun initViews() {
        binding.nameTextView.text = viewModel.getUserLocal().first_name
        binding.profileImageView.setOnClickListener {

            toUser()
        }
    }

    private fun toUser() {
        val intent = Intent(this, UserActivity::class.java)
        startActivity(intent)
    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.logout -> {
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

}