package com.fernt0411.myproduct.ui.main.card

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fernt0411.myproduct.data.c_domain.model.Card
import com.fernt0411.myproduct.data.c_domain.model.CreditPetition
import com.fernt0411.myproduct.data.c_domain.model.StatusOk
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetCardSettingsUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.UpdateForeignSettingStateUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.UpdateInternetSettingStateUseCase
import com.fernt0411.myproduct.utils.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class CardViewModel @ViewModelInject constructor(
    private var getCardSettingsUseCase: GetCardSettingsUseCase,
    private var updateInternetSettingStateUseCase: UpdateInternetSettingStateUseCase,
    private var updateForeignSettingStateUseCase: UpdateForeignSettingStateUseCase


) : ViewModel() {

    private var creditPetition: CreditPetition = CreditPetition()


    private val _getCardSettingsResult = MutableLiveData<Resource<Card>>()
    val getCardSettingsResult: LiveData<Resource<Card>> get() = _getCardSettingsResult

    private val _updateInternetSettingStatResult = MutableLiveData<Resource<StatusOk>>()
    val updateInternetSettingStatResult: LiveData<Resource<StatusOk>> get() = _updateInternetSettingStatResult

    private val _updateForeignSettingStateResult = MutableLiveData<Resource<StatusOk>>()
    val updateForeignSettingStateResult: LiveData<Resource<StatusOk>> get() = _updateForeignSettingStateResult


    init {
        onGetCardSettings()
    }


    fun onGetCardSettings() {
        viewModelScope.launch {
            _getCardSettingsResult.value = Resource.loading()
            _getCardSettingsResult.value = withContext(Dispatchers.IO) {

                getCardSettingsUseCase.invoke()
            }
        }
    }

    /*
    fun onUpdateInternetSetting() {
        viewModelScope.launch {
            _updateInternetSettingStatResult.value = Resource.loading()
            _updateInternetSettingStatResult.value = withContext(Dispatchers.IO) {

                updateInternetSettingStateUseCase.invoke()
            }
        }
    }
   */
    fun onUpdateForeignSetting(isActive:Int) {
        viewModelScope.launch {
            _updateForeignSettingStateResult.value = Resource.loading()
            _updateForeignSettingStateResult.value = withContext(Dispatchers.IO) {

                updateForeignSettingStateUseCase.invoke(isActive)
            }
        }
    }




}







