package com.fernt0411.myproduct.ui.start

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fernt0411.myproduct.MyProductsApp
import com.fernt0411.myproduct.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class StartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_presentation)

    }


}