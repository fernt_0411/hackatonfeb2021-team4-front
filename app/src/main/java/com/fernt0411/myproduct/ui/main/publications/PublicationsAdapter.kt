package com.fernt0411.myproduct.ui.main.publications


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.Publication
import com.fernt0411.myproduct.utils.ui.ItemClickListener
import com.fernt0411.myproduct.utils.ui.onClick
import kotlinx.android.synthetic.main.item_publication.view.*

class PublicationsAdapter() : RecyclerView.Adapter<PublicationsAdapter.ViewHolder>() {

    lateinit var itemClickListener: ItemClickListener<Publication>

    var list: List<Publication> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()

        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_publication, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val transaction = list[position]
        holder.bindPublication(transaction)

        holder.itemView.onClick { itemClickListener(position, transaction) }

    }

    override fun getItemCount(): Int {
        //Aqui indicas cuantas filas tendra tu RecyclerView
        return list.size
    }


    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bindPublication(transaction: Publication) {
            view.descriptionTextView.text = transaction.description
            view.authorTextView.text = transaction.name +" "+ transaction.lastname

            view.dateTextView.text = transaction.date

            var formatAmount: String
            if (transaction.type == "Donacion") {

                view.typeTextView.setTextColor(view.context.resources.getColor(R.color.colorIncome))

            } else {

                view.typeTextView.setTextColor(view.context.resources.getColor(R.color.colorExpenses))
            }
            view.typeTextView.text= transaction.type
        }

    }

}