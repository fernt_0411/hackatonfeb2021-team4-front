package com.fernt0411.myproduct.ui.main.user

import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.fernt0411.myproduct.MyProductsApp
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.databinding.ActivityUserBinding
import com.fernt0411.myproduct.ui.main.MainViewModel
import com.fernt0411.myproduct.utils.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class UserActivity : BaseActivity() {
    val viewModel: UserViewModel by viewModels()
    private lateinit var binding: ActivityUserBinding
    private var user: User = User()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_user)
        setContentView(binding.root)


        initViews()
    }


    private fun initViews() {
        user = viewModel.getUserLocal()
        user.let {
            binding.nameTextView.text= it.first_name
            binding.lastNameTextView.text= it.last_name
            binding.emailTextView.text= it.email

        }

    }

}