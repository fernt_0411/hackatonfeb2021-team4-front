package com.fernt0411.myproduct.ui.main.currencyexchange

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*

import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.data.c_domain.model.Simulation
import com.fernt0411.myproduct.data.c_domain.model.SimulationPetition
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.d_usecase.auth.LoginUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetExchangeSimulationUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetResumeUseCase
import com.fernt0411.myproduct.data.d_usecase.user.SaveUserLocalUseCase
import com.fernt0411.myproduct.utils.response.Resource
import com.fernt0411.myproduct.utils.validators.FormValidators
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class CurrencyExchangeViewModel @ViewModelInject constructor(

    private val getExchangeSimulationUseCase: GetExchangeSimulationUseCase

) : ViewModel() {

    private var simulationPetition: SimulationPetition = SimulationPetition()


    private val _simulationResult = MutableLiveData<Resource<Simulation>>()
    val simulationResult: LiveData<Resource<Simulation>> get() = _simulationResult


    val amount = MutableLiveData("")

    val sell = MutableLiveData(0)

    init {
        onValidateForm(amount = "")
    }

    val isFormValid = MediatorLiveData<Boolean>().apply {
        addSource(amount) {
            value = onValidateForm(amount.value.toString())
            Log.d("user_pass", value.toString())
        }
    }


    fun onCalculate() {
        viewModelScope.launch {
            _simulationResult.value = Resource.loading()
            _simulationResult.value = withContext(Dispatchers.IO) {

                simulationPetition.amount = amount.value?.toDouble() ?: 0.0
                simulationPetition.sell = sell.value?.toInt() ?: 1
                getExchangeSimulationUseCase.invoke(simulationPetition)
            }
        }
    }


    //crear utils para forms y fields
    private fun onValidateForm(amount: String): Boolean {
        return amount.isNotEmpty()
    }


    fun isSell() = sell.value == 1



}







