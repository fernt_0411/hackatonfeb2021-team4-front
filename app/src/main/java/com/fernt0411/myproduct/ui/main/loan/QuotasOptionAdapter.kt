package com.fernt0411.myproduct.ui.main.loan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.QuotaOption
import com.fernt0411.myproduct.utils.ui.ItemClickListener
import com.fernt0411.myproduct.utils.ui.onClick


class QuotasOptionAdapter() : RecyclerView.Adapter<QuotasOptionAdapter.ViewHolder>() {


    lateinit var itemClickListener: ItemClickListener<QuotaOption>

    var quotas: ArrayList<QuotaOption> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()

        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_quota_option, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val quota = this.quotas[position]
        holder.bind(quota)
        holder.itemView.onClick { itemClickListener(position, quota) }
    }

    override fun getItemCount(): Int {
        return quotas.size
    }


    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(quotaOption: QuotaOption) {

            view.findViewById<TextView>(R.id.descriptionQuotaTextView).text =
                quotaOption.description
            //view.descriptionQuotaTextView.text = quotaOption.description


        }

    }
}