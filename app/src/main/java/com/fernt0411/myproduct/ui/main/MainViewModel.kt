package com.fernt0411.myproduct.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCase

class MainViewModel @ViewModelInject constructor(

    private val getUserLocalUseCase: GetUserLocalUseCase

) : ViewModel() {


    fun getUserLocal(): User {
        return getUserLocalUseCase.invoke()
    }

}