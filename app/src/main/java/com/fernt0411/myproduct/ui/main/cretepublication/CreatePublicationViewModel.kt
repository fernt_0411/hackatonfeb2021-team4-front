package com.fernt0411.myproduct.ui.main.cretepublication

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fernt0411.myproduct.data.c_domain.model.History
import com.fernt0411.myproduct.data.c_domain.model.SimulationPetition
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.c_domain.req.CreatePublicationReq
import com.fernt0411.myproduct.data.c_domain.res.StatusOkRes
import com.fernt0411.myproduct.data.d_usecase.dashboard.CreatePublicationUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetHistoryUseCase
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCase
import com.fernt0411.myproduct.utils.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class CreatePublicationViewModel @ViewModelInject constructor(


    private val createPublicationUseCase: CreatePublicationUseCase,
    private val getUserLocalUseCase: GetUserLocalUseCase

) : ViewModel() {



    private val _createPublicationResult = MutableLiveData<Resource<CreatePublicationReq>>()
    val createPublicationResult: LiveData<Resource<CreatePublicationReq>> get() = _createPublicationResult


    val description = MutableLiveData("")

    val sell = MutableLiveData(0)

    init {
        onValidateForm(description = "")
      //  onGetHistory()
    }

    val isFormValid = MediatorLiveData<Boolean>().apply {
        addSource(description) {
            value = onValidateForm(description.value.toString())
            Log.d("user_pass", value.toString())
        }
    }


    fun onCreatePublication(createPublicationReq: CreatePublicationReq) {
        viewModelScope.launch {
            _createPublicationResult.value = Resource.loading()
            _createPublicationResult.value = withContext(Dispatchers.IO) {

                createPublicationUseCase.invoke(createPublicationReq)
            }
        }
    }


    //crear utils para forms y fields
    private fun onValidateForm(description: String): Boolean {
        return description.isNotEmpty()
    }

     fun onGetUser():User{

        return getUserLocalUseCase.invoke()
    }


}







