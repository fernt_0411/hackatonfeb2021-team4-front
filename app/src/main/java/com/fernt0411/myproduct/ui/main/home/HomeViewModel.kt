package com.fernt0411.myproduct.ui.main.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fernt0411.myproduct.data.c_domain.model.Inquiry
import com.fernt0411.myproduct.data.c_domain.model.Resume
import com.fernt0411.myproduct.data.c_domain.model.StatusOk
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetInquiryUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.SaveInquiryUseCase
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCase
import com.fernt0411.myproduct.utils.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class HomeViewModel @ViewModelInject constructor(
    private val getUserLocalUseCase: GetUserLocalUseCase,


) : ViewModel() {

    private val _getInquiryResult = MutableLiveData<Resource<Inquiry>>()
    val getInquiryResult: LiveData<Resource<Inquiry>> get() = _getInquiryResult



    fun getUserLocal(): User {
        return getUserLocalUseCase.invoke()
    }



}







