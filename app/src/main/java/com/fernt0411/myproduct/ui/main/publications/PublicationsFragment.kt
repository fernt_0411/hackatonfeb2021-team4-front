package com.fernt0411.myproduct.ui.main.publications


import android.content.Intent
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.Publication
import com.fernt0411.myproduct.databinding.FragmentPublicationsBinding
import com.fernt0411.myproduct.ui.main.cretepublication.CreatePublicationActivity
import com.fernt0411.myproduct.utils.base.BaseFragment
import com.fernt0411.myproduct.utils.response.Resource
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class PublicationsFragment :
    BaseFragment<PublicationsViewModel, FragmentPublicationsBinding>() {
    override val viewModel: PublicationsViewModel by viewModels()
    override fun getLayoutRes(): Int = R.layout.fragment_publications

    private lateinit var publicationRecyclerView: RecyclerView


    private lateinit var adapter: PublicationsAdapter
    private var publicationList: MutableList<Publication> = ArrayList()
    private lateinit var incomeChip: Chip
    private lateinit var expensesChip: Chip
    private lateinit var allChip: Chip
    private lateinit var filtersGroup: ChipGroup
    private lateinit var createPublicationButton: FloatingActionButton

    private var isCheckedExpenses: Boolean = true
    private var isCheckedIncome: Boolean = true

    override fun initialize() {
        super.initialize()
        initViews()
        initEvents()
        onSubscribeViewModel()

    }


    private fun onSubscribeViewModel() {
        viewModel.getPublications.observe(viewLifecycleOwner, Observer {

            when (it.status) {
                Resource.Status.LOADING -> showFullscreenLoader()
                Resource.Status.SUCCESS -> {


                    Log.d("miclase", it.data.toString())
                    if (it.data != null) {
                        publicationList = it.data!!
                        setTransactions()
                    } else {

                    }

                    hideFullscreenLoader()
                }
                Resource.Status.ERROR -> {

                    hideFullscreenLoader()
                    Log.d("miclase", it.data.toString())
                }
            }

        })


    }

    override fun onResume() {
        super.onResume()
        viewModel.onGetPublications()

    }

    private fun initViews() {
        setRecyclerView()
        filtersGroup = binding.filtersGroup
        allChip = binding.allChip
        incomeChip = binding.incomeChip
        expensesChip = binding.expensesChip
        createPublicationButton = binding.createPublicationButton
    }

    private fun setRecyclerView() {
        // publicationList.add(
        //     Publication(
        //         "12//12/21",
        //         "Fernando",
        //         "Tupac yupanqui",
        //         "Hola esta es una decripcion",
        //         "Donacion",
        //         "local",
        //         "av tupac amaru 2963",
        //         -12.3434,
        //         -12.44344
        //     )
        // )
        // publicationList.add(
        //     Publication(
        //         "12//12/21",
        //         "Fernando",
        //         "Tupac yupanqui",
        //         "Hola esta es una decripcion",
        //         "Donacion",
        //         "local",
        //         "av tupac amaru 2963",
        //         -12.3434,
        //         -12.44344
        //     )
        // )
        // publicationList.add(
        //     Publication(
        //         "12//12/21",
        //         "Fernando",
        //         "Tupac yupanqui",
        //         "Hola esta es una decripcion",
        //         "Donacion",
        //         "local",
        //         "av tupac amaru 2963",
        //         -12.3434,
        //         -12.44344
        //     )
        // )
        // publicationList.add(
        //     Publication(
        //         "12//12/21",
        //         "Fernando",
        //         "Tupac yupanqui",
        //         "Hola esta es una decripcion",
        //         "Donacion",
        //         "local",
        //         "av tupac amaru 2963",
        //         -12.3434,
        //         -12.44344
        //     )
        // )
        adapter = PublicationsAdapter()
        publicationRecyclerView = binding.publicationRecyclerView
        adapter.itemClickListener = { _, data ->

        }

        publicationRecyclerView.layoutManager = LinearLayoutManager(context)
        publicationRecyclerView.adapter = adapter
        adapter.list = publicationList

    }


    private fun initEvents() {

        createPublicationButton.setOnClickListener {

            val intent = Intent(context, CreatePublicationActivity::class.java)
            startActivity(intent)
        }

        allChip.setOnCheckedChangeListener { chip, isChecked ->

            if (isChecked) {
                setTransactions()
                chip.isClickable = false
                incomeChip.isChecked = false
                expensesChip.isChecked = false
                incomeChip.isClickable = true
                expensesChip.isClickable = true
            }

        }

        incomeChip.setOnCheckedChangeListener { chip, isChecked ->

            if (isChecked) {
                setTransactions("Donacion")
                chip.isClickable = false
                allChip.isChecked = false
                expensesChip.isChecked = false
                allChip.isClickable = true
                expensesChip.isClickable = true
            }

        }

        expensesChip.setOnCheckedChangeListener { chip, isChecked ->

            if (isChecked) {
                setTransactions("Recepcion")
                chip.isClickable = false
                allChip.isChecked = false
                incomeChip.isChecked = false
                allChip.isClickable = true
                incomeChip.isClickable = true
            }


        }


    }

    private fun setTransactions(filter: String? = null) {

        if (filter == null) {
            adapter.list = publicationList
        } else {
            adapter.list = filterTransactions(filter)
        }
    }

    private fun filterTransactions(typeFilter: String): List<Publication> {
        return publicationList.filter { it.type == typeFilter }

    }

}




