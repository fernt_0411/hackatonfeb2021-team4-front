package com.fernt0411.myproduct.ui.main.loan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.Quota
import com.fernt0411.myproduct.utils.ui.ItemClickListener
import kotlinx.android.synthetic.main.item_quota.view.*


class CreditSimulationAdapter() : RecyclerView.Adapter<CreditSimulationAdapter.ViewHolder>() {
    lateinit var itemClickListener: ItemClickListener<Quota>

    var schedule: ArrayList<Quota> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()

        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_quota, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val quota = this.schedule[position]
        holder.bind(quota)
        //holder.itemView.onClick { itemClickListener(position, quota) }
    }

    override fun getItemCount(): Int {
        return schedule.size
    }


    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(quota: Quota) {
            view.monthTextView.text = quota.month
            view.amountTextView.text = String.format("%.3f", quota.amount)


        }

    }
}