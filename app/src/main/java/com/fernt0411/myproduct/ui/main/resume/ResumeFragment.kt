package com.fernt0411.myproduct.ui.main.resume

import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.Month
import com.fernt0411.myproduct.data.c_domain.model.Resume
import com.fernt0411.myproduct.databinding.FragmentResumeBinding
import com.fernt0411.myproduct.utils.base.BaseFragment
import com.fernt0411.myproduct.utils.response.Resource
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartModel
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartType
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartView
import com.github.aachartmodel.aainfographics.aachartcreator.AASeriesElement
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class ResumeFragment : BaseFragment<ResumeViewModel, FragmentResumeBinding>() {


    override val viewModel: ResumeViewModel by viewModels()
    override fun getLayoutRes(): Int = R.layout.fragment_resume

    private lateinit var resumeLineChart: AAChartView
    private lateinit var listResume: Resume
    private val aaChartModel: AAChartModel = AAChartModel()
    override fun initialize() {
        super.initialize()
        initViews()
        onSubscribeViewModel()
        buildGraph()
    }

    private fun buildGraph() {

        aaChartModel
            .chartType(AAChartType.Column)
            .title("Resumen total por mes")
            .yAxisTitle("Soles")
            .backgroundColor("#ffffff")
            .dataLabelsEnabled(true)
            .touchEventEnabled(true)

        resumeLineChart = binding.resumeChartView



        resumeLineChart.aa_drawChartWithChartModel(aaChartModel)


    }

    private fun initViews() {


    }

    private fun onSubscribeViewModel() {
        viewModel.getResumeResult.observe(viewLifecycleOwner, Observer {

            when (it.status) {
                Resource.Status.LOADING -> showFullscreenLoader()
                Resource.Status.SUCCESS -> {
                    hideFullscreenLoader()
                    it.data?.let { listResume ->
                        this.listResume = listResume

                    }


                    var listR = Array<Any>(listResume.months.size) { number ->
                        number.toDouble()
                    }

                    val listL = Array(listResume.months.size) { number ->
                        "n = $number"
                    }



                    listResume.months.forEachIndexed { i: Int, month: Month ->
                        var total =
                            (month.credits + month.expenses + month.income + month.assets + month.loans).toDouble()
                        listL[i] = month.monthDescription
                        listR[i] = total
                    }

                    aaChartModel.categories = listL
                    resumeLineChart.aa_refreshChartWithChartModel(aaChartModel)




                    resumeLineChart.aa_addElementToChartSeries(
                        AASeriesElement().data(listR).name("total en soles")
                    )


                }
                Resource.Status.ERROR -> {
                    hideFullscreenLoader()

                }
            }

        })


    }

}


