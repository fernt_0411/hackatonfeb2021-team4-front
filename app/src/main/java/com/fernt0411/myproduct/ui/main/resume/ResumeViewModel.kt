package com.fernt0411.myproduct.ui.main.resume

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fernt0411.myproduct.data.c_domain.model.Resume
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetResumeUseCase
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCase
import com.fernt0411.myproduct.utils.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ResumeViewModel @ViewModelInject constructor(

    private val getResumeUseCase: GetResumeUseCase

) : ViewModel() {

    private val _getResumeResult = MutableLiveData<Resource<Resume>>()
    val getResumeResult: LiveData<Resource<Resume>> get() = _getResumeResult


    init {

        viewModelScope.launch {
            _getResumeResult.value = Resource.loading()
            _getResumeResult.value = withContext(Dispatchers.IO) {
                getResumeUseCase.invoke()
            }

            Log.d("fernando", _getResumeResult.value.toString())
        }

    }

}