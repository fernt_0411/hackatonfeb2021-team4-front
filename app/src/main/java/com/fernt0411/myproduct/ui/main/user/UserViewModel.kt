package com.fernt0411.myproduct.ui.main.user

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.d_usecase.user.GetUserLocalUseCase

class UserViewModel @ViewModelInject constructor(

    private val getUserLocalUseCase: GetUserLocalUseCase

) : ViewModel() {


    fun getUserLocal(): User {
        return getUserLocalUseCase.invoke()
    }

}