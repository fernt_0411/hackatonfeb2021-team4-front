package com.fernt0411.myproduct.ui.main.currencyexchange


import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.Simulation
import com.fernt0411.myproduct.databinding.FragmentCurrencyExchangeBinding
import com.fernt0411.myproduct.utils.base.BaseFragment
import com.fernt0411.myproduct.utils.response.Resource
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class CurrencyExchangeFragment :
    BaseFragment<CurrencyExchangeViewModel, FragmentCurrencyExchangeBinding>() {
    override val viewModel: CurrencyExchangeViewModel by viewModels()
    override fun getLayoutRes(): Int = R.layout.fragment_currency_exchange

    private lateinit var conversionTextView: TextView
    private lateinit var currencyExchangeTextView: TextView
    private lateinit var calculateButton: MaterialButton
    private lateinit var purchaseChip: Chip
    private lateinit var sellChip: Chip

    private var simulation: Simulation = Simulation()


    override fun initialize() {
        super.initialize()
        initViews()
        initEvents()
        onSubscribeViewModel()

    }

    private fun onSubscribeViewModel() {
        viewModel.simulationResult.observe(viewLifecycleOwner, Observer {

            when (it.status) {
                Resource.Status.LOADING -> showFullscreenLoader()
                Resource.Status.SUCCESS -> {


                    Log.d("miclase", it.data.toString())
                    simulation = it.data!!
                    setSimulation()

                    hideFullscreenLoader()
                }
                Resource.Status.ERROR -> Log.d("miclase", it.data.toString())
            }

        })


    }

    private fun initViews() {

        conversionTextView = binding.conversionTextView
        currencyExchangeTextView = binding.currencyExchangeTextView

        conversionTextView.visibility = View.GONE
        currencyExchangeTextView.visibility = View.GONE

        calculateButton = binding.calculateButton
        purchaseChip = binding.purchaseChip
        sellChip = binding.sellChip


    }

    private fun initEvents() {
        calculateButton.setOnClickListener {
            viewModel.onCalculate()
        }

        purchaseChip.setOnCheckedChangeListener { chip, isChecked ->


            if (isChecked) {
                viewModel.sell.value = 0
                chip.isClickable = false
                sellChip.isChecked = false
                sellChip.isClickable = true

            }
        }
        sellChip.setOnCheckedChangeListener { chip, isChecked ->


            if (isChecked) {
                viewModel.sell.value = 1
                chip.isClickable = false
                purchaseChip.isChecked = false
                purchaseChip.isClickable = true


            }
        }


    }

    private fun setSimulation() {


        currencyExchangeTextView.text =
            getString(R.string.currency_exchange, simulation.exchange)
        conversionTextView.text =
            getString(R.string.conversion, simulation.simulationAmount.toString())


        conversionTextView.visibility = View.VISIBLE
        currencyExchangeTextView.visibility = View.VISIBLE


    }

}




