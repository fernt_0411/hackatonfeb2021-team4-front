package com.fernt0411.myproduct.ui.main.loan


import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.QuotaOption
import com.fernt0411.myproduct.data.c_domain.model.SimulationCredit
import com.fernt0411.myproduct.databinding.FragmentLoanBinding
import com.fernt0411.myproduct.utils.base.BaseFragment
import com.fernt0411.myproduct.utils.response.Resource
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class LoanFragment :
    BaseFragment<LoanViewModel, FragmentLoanBinding>() {
    override val viewModel: LoanViewModel by viewModels()
    override fun getLayoutRes(): Int = R.layout.fragment_loan

    private lateinit var creditSimulationDialog: AlertDialog
    private lateinit var calculateButton: MaterialButton

    private lateinit var quotasDialog: AlertDialog
    private lateinit var quotaEditText: EditText
    private lateinit var messageConstraintLayout: ConstraintLayout
    private lateinit var messageTextView: TextView


    private var simulationCredit: SimulationCredit = SimulationCredit()
    private var quotasOption: ArrayList<QuotaOption> = ArrayList()


    override fun initialize() {
        super.initialize()
        initViews()

        initEvents()
        onSubscribeViewModel()

    }


    private fun onSubscribeViewModel() {
        viewModel.getCreditSimulation.observe(viewLifecycleOwner, Observer {
            messageConstraintLayout.visibility = View.GONE
            when (it.status) {
                Resource.Status.LOADING -> showFullscreenLoader()
                Resource.Status.SUCCESS -> {

                    it.data?.let { simulationCredit ->
                        this.simulationCredit = simulationCredit
                    }
                    messageConstraintLayout.visibility = View.VISIBLE
                    if (simulationCredit.loan) {

                        messageTextView.text =simulationCredit.message
                        context?.resources?.getColor(R.color.colorIncome)?.let { it1 ->
                            messageConstraintLayout.setBackgroundColor(
                                it1
                            )
                        }
                        showCreditSimulationDialog()
                    } else {
                        messageTextView.text = simulationCredit.message
                        context?.resources?.getColor(R.color.colorExpenses)?.let { it1 ->
                            messageConstraintLayout.setBackgroundColor(
                                it1
                            )
                        }
                    }

                    hideFullscreenLoader()

                }
                Resource.Status.ERROR -> Log.d("miclase", it.data.toString())
            }

        })


    }

    private fun initViews() {
        calculateButton = binding.calculateButton
        quotaEditText = binding.quotaEditText
        messageConstraintLayout = binding.messageConstraintLayout
        messageTextView = binding.messageTextView
        setQuotas()

    }

    private fun initEvents() {

        quotaEditText.setOnClickListener {
            showQuotasDialog()
        }


        calculateButton.setOnClickListener {
            viewModel.onCalculate()
        }
    }


    private fun setQuotas() {
        quotasOption.add(QuotaOption("12 meses", 12))
        quotasOption.add(QuotaOption("18 meses", 18))
        quotasOption.add(QuotaOption("24 meses", 24))
        quotasOption.add(QuotaOption("36 meses", 36))
        quotasOption.add(QuotaOption("48 meses", 48))
    }


    private fun showCreditSimulationDialog() {
        if (!::creditSimulationDialog.isInitialized) {
            val view = layoutInflater.inflate(R.layout.credit_simulation_dialog, null)
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.setView(view)

            val adapter = CreditSimulationAdapter()
            val recyclerView: RecyclerView = view.findViewById(R.id.scheduleRecyclerView)

            var toolbar: MaterialToolbar =
                view.findViewById(R.id.topAppBar)

            toolbar.setOnMenuItemClickListener {
                when (it.itemId) {

                    R.id.close -> creditSimulationDialog.dismiss()
                }
                true
            }

            recyclerView.adapter = adapter
            adapter.schedule = simulationCredit.schedule
            creditSimulationDialog = alertDialog.create()
        }
        creditSimulationDialog.show()
    }


    private fun showQuotasDialog() {
        if (!::quotasDialog.isInitialized) {
            val view = layoutInflater.inflate(R.layout.quotas_dialog, null)
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.setView(view)

            val adapter = QuotasOptionAdapter()
            val recyclerView: RecyclerView = view.findViewById(R.id.quotasOptionRecyclerView)
            recyclerView.adapter = adapter



            adapter.itemClickListener = { _, data ->
                viewModel.quota.value = data.value.toString()
                quotasDialog.dismiss()
            }

            recyclerView.adapter = adapter
            adapter.quotas = quotasOption
            quotasDialog = alertDialog.create()
        }
        quotasDialog.show()

    }

}




