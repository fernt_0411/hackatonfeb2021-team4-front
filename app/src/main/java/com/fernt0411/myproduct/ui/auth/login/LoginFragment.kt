package com.fernt0411.myproduct.ui.auth.login


import android.content.Intent
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.databinding.FragmentLoginBinding
import com.fernt0411.myproduct.ui.main.MainActivity
import com.fernt0411.myproduct.utils.base.BaseFragment
import com.fernt0411.myproduct.utils.response.Resource

import com.google.android.material.button.MaterialButton

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class LoginFragment :
    BaseFragment<LoginViewModel, FragmentLoginBinding>() {
    override val viewModel: LoginViewModel by viewModels()
    override fun getLayoutRes(): Int = R.layout.fragment_login

    private lateinit var loginButton: MaterialButton


    override fun initialize() {
        super.initialize()
        initEvents()
        onSubscribeViewModel()

    }

    private fun onSubscribeViewModel() {
        viewModel.loginResult.observe(viewLifecycleOwner, Observer {

            when (it.status) {
                Resource.Status.LOADING -> showFullscreenLoader()
                Resource.Status.SUCCESS -> {
                    Log.d("miclase", it.data.toString())


                    //it.data?.user?.let { localUser -> saveUserLocal(localUser) }

                    activity?.finish()

                    val intent = Intent(context, MainActivity::class.java)
                    startActivity(intent)
                    hideFullscreenLoader()
                    /*view?.let {
                         Navigation.findNavController(it)
                             .navigate(R.id.action_loginFragment_to_mainActivity)
                     }*/

                }
                Resource.Status.ERROR -> Log.d("miclase", it.data.toString())
            }

        })


    }

    private fun initEvents() {
        onLogin()
    }

    private fun onLogin() {
        loginButton = binding.loginButton
        loginButton.setOnClickListener {
            onLogin()
            val intent = Intent(context, MainActivity::class.java)
            startActivity(intent)
            //viewModel.onLogin()

        }
    }


}




