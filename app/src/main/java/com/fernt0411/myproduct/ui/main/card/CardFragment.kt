package com.fernt0411.myproduct.ui.main.card


import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.fernt0411.myproduct.R
import com.fernt0411.myproduct.data.c_domain.model.Card
import com.fernt0411.myproduct.databinding.FragmentCardBinding
import com.fernt0411.myproduct.utils.base.BaseFragment
import com.fernt0411.myproduct.utils.response.Resource
import com.google.android.material.switchmaterial.SwitchMaterial
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class CardFragment :
    BaseFragment<CardViewModel, FragmentCardBinding>() {
    override val viewModel: CardViewModel by viewModels()
    override fun getLayoutRes(): Int = R.layout.fragment_card

    private lateinit var internetSwitch: SwitchMaterial
    private lateinit var foreignSwitch: SwitchMaterial
    private var card: Card = Card()

    override fun initialize() {
        super.initialize()
        initViews()
        initEvents()
        onSubscribeViewModel()

    }


    private fun onSubscribeViewModel() {
        viewModel.getCardSettingsResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.LOADING -> {

                    internetSwitch.isEnabled = false
                    foreignSwitch.isEnabled = false
                    showFullscreenLoader()
                }
                Resource.Status.SUCCESS -> {
                    card = it.data!!
                    internetSwitch.isEnabled = true
                    foreignSwitch.isEnabled = true
                    setCardSettings()
                    hideFullscreenLoader()
                }
                Resource.Status.ERROR -> Log.d("miclase", it.data.toString())
            }

        })

        viewModel.updateForeignSettingStateResult.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.LOADING -> foreignSwitch.isEnabled = false
                Resource.Status.SUCCESS -> {
                    foreignSwitch.isEnabled = true

                }
                Resource.Status.ERROR -> {
                    foreignSwitch.isEnabled = true
                    Log.d("miclase", it.data.toString())
                }
            }

        })


    }

    private fun setCardSettings() {
        internetSwitch.isChecked = card.cardSettings.cardInternet
        foreignSwitch.isChecked = card.cardSettings.cardForeign
    }

    private fun initViews() {
        internetSwitch = binding.internetSwitch
        foreignSwitch = binding.foreignSwitch
    }

    private fun initEvents() {
        internetSwitch.setOnCheckedChangeListener { buttonView, isChecked ->


        }

        foreignSwitch.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                viewModel.onUpdateForeignSetting(1)
            } else {
                viewModel.onUpdateForeignSetting(0)
            }


        }

    }


}




