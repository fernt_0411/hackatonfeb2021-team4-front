package com.fernt0411.myproduct.ui.auth.login

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*

import com.fernt0411.myproduct.data.c_domain.model.Auth
import com.fernt0411.myproduct.data.c_domain.model.User
import com.fernt0411.myproduct.data.d_usecase.auth.LoginUseCase
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetResumeUseCase
import com.fernt0411.myproduct.data.d_usecase.user.SaveUserLocalUseCase
import com.fernt0411.myproduct.utils.response.Resource
import com.fernt0411.myproduct.utils.validators.FormValidators
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class LoginViewModel @ViewModelInject constructor(
    private val loginUseCase: LoginUseCase,
    private val saveUserLocalUseCase: SaveUserLocalUseCase

) : ViewModel() {
    // TODO: Implement the ViewModel



    val dni = MutableLiveData("")
    val password = MutableLiveData("")


    private val _loginResult = MutableLiveData<Resource<Auth>>()
    val loginResult: LiveData<Resource<Auth>> get() = _loginResult

    init {
        onValidateForm(dni = "", password = "")
    }

    val isFormValid = MediatorLiveData<Boolean>().apply {
        addSource(dni) {
            value = onValidateForm(dni.value.toString(), password.value.toString())
            Log.d("user_pass", value.toString())
        }
        addSource(password) {
            value = onValidateForm(dni.value.toString(), password.value.toString())
            Log.d("custom_pass", value.toString())
        }
    }


    fun onLogin() {
        saveUserLocalUseCase.invoke(User("Fernando","Tupac yupanqui","Fernt0411@gmail.com"))
       //viewModelScope.launch {
       //    _loginResult.value = Resource.loading()
       //    _loginResult.value = withContext(Dispatchers.IO) {
       //        loginUseCase.invoke(dni.value.toString(), password.value.toString())
       //    }
       //}
    }



    //crear utils para forms y fields
    private fun onValidateForm(dni: String, password: String): Boolean {
        return dni.isNotEmpty() && password.isNotEmpty()
    }

}







