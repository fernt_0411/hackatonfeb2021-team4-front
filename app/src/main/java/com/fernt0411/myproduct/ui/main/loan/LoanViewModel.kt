package com.fernt0411.myproduct.ui.main.loan

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.fernt0411.myproduct.data.c_domain.model.CreditPetition
import com.fernt0411.myproduct.data.c_domain.model.SimulationCredit
import com.fernt0411.myproduct.data.d_usecase.dashboard.GetSimulationCreditUseCase
import com.fernt0411.myproduct.utils.response.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
class LoanViewModel @ViewModelInject constructor(

    private var getSimulationCreditUseCase: GetSimulationCreditUseCase

) : ViewModel() {

    private var creditPetition: CreditPetition = CreditPetition()


    private val _getCreditSimulation = MutableLiveData<Resource<SimulationCredit>>()
    val getCreditSimulation: LiveData<Resource<SimulationCredit>> get() = _getCreditSimulation


    val amount = MutableLiveData("")

    val quota = MutableLiveData("")

    init {
        onValidateForm(amount = "", quota = "")
    }

    val isFormValid = MediatorLiveData<Boolean>().apply {
        addSource(amount) {
            value = onValidateForm(amount.value.toString(), quota.value.toString())
            Log.d("user_pass", value.toString())
        }
        addSource(quota) {
            value = onValidateForm(amount.value.toString(), quota.value.toString())
            Log.d("user_pass", value.toString())
        }
    }


    fun onCalculate() {
        viewModelScope.launch {
            _getCreditSimulation.value = Resource.loading()
            _getCreditSimulation.value = withContext(Dispatchers.IO) {


                creditPetition.quota = quota.value?.toInt() ?: 0
                creditPetition.amount = amount.value?.toDouble() ?: 0.0

                getSimulationCreditUseCase.invoke(creditPetition)


            }
        }
    }


    //crear utils para forms y fields
    private fun onValidateForm(amount: String, quota: String): Boolean {
        return amount.isNotEmpty() && quota.isNotEmpty()
    }


}







