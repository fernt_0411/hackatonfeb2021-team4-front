package com.fernt0411.myproduct.ui.auth.login

import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
internal class TransactionsViewModelTest {
/*
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule() //para probar live data

    @get:Rule
    val testCoroutineRule =
        TestCoroutineRule() //tenemos el control total de ejecutar todo en TestCoroutineDispatcher.

    @Mock
    private lateinit var listUsersUseCase: ListUsersUseCase


    private lateinit var viewModel: LoginViewModel


    @Before
    fun setup() {
        // do something if required
        MockitoAnnotations.initMocks(this)
        viewModel = LoginViewModel(listUsersUseCase)
    }


    @Test
    fun `should get more than one user when invoke listUsersUseCase`() {

        var users = listOf<User>(
            User("1", "fer@gmail.com","fer","12/12/2020")
        )
        var resource = Resource.success(users)

        testCoroutineRule.runBlockingTest {
            //Given
            `when`(listUsersUseCase.invoke()).thenReturn(resource);

            //When
            viewModel.getUsers()

            //Then
            val value = viewModel.users.getOrAwaitValue()
     //       assertThat(value.data?.size, Matchers.hasSize(4))

        }


    }



    @Test
    fun `should get error when invoke`() {


        var resource = Resource.error(message = "is an error",null)

        testCoroutineRule.runBlockingTest {
            //Given
            `when`(listUsersUseCase.invoke()).thenReturn(resource);

            //When
            viewModel.getUsers()

            //Then
            assertThat(viewModel.users.getOrAwaitValue().status, Matchers.`is`(resource.status))
        }


    }
*/

}



